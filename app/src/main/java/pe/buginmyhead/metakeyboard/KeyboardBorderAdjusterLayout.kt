package pe.buginmyhead.metakeyboard

import android.content.Context
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.MotionEvent
import android.widget.LinearLayout
import androidx.preference.PreferenceManager
import kotlin.math.roundToInt

class KeyboardBorderAdjusterLayout @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
  var removeViewFromWindow: () -> Unit = {}

  private var keyboardHeight =
      PreferenceManager
          .getDefaultSharedPreferences(context)
          .getInt("keyboard_height", resources.getInteger(R.integer.pref_keyboard_height_default))

  override fun dispatchKeyEvent(event: KeyEvent): Boolean {
    return if (event.action == KeyEvent.ACTION_UP && event.keyCode == KeyEvent.KEYCODE_BACK) {
      removeViewFromWindow()
      true
    } else {
      super.dispatchKeyEvent(event)
    }
  }

  override fun dispatchTouchEvent(event: MotionEvent): Boolean {
    keyboardHeight = height - event.y.roundToInt()
    PreferenceManager
        .getDefaultSharedPreferences(context)
        .edit()
        .putInt("keyboard_height", keyboardHeight)
        .apply()
    invalidate()
    if (event.action == MotionEvent.ACTION_UP) {
      removeViewFromWindow()
    }
    return true
  }
}