package pe.buginmyhead.metakeyboard

import kotlin.reflect.KClass
import kotlin.reflect.KVisibility
import kotlin.reflect.full.memberFunctions

object MetakeyboardModel {
  var integrationComposer: IntegrationComposer = MetakeyboardOfficialIntegrationComposer()

  private val functions =
    this::class.memberFunctions.filter { it.visibility == KVisibility.PUBLIC }.associateBy { it.name }
  private val variables =
    mutableMapOf<String, String>()
  private val languageComposerOptions =
    mutableMapOf<KClass<out LanguageComposer>, MutableMap<String, String>>()
  private val integrationComposerOptions =
    mutableMapOf<KClass<out IntegrationComposer>, MutableMap<String, String>>()

  var showKeyboardListCallback: () -> Unit = {}
  var openAppCallback: (List<String>) -> Unit = { args: List<String> -> }
  var showSettingsCallback: () -> Unit = {}
  var adjustKeyboardBorderCallback: () -> Unit = {}

  fun call(callee: Callee) {
    callee.arguments.map {
      when (it.type) {
        Callee.Argument.Type.RAW -> it.value
        Callee.Argument.Type.MAP -> variables[it.value]!!
      }
    }.also {
      when (callee.functionName) {
        "show_keyboard_list" -> showKeyboardListCallback()
        "open_app" -> openAppCallback(it)
        "show_settings" -> showSettingsCallback()
        "adjust_keyboard_border" -> adjustKeyboardBorderCallback()
        else -> functions[callee.functionName]?.call(this, it)
      }
    }
  }

  fun setLocaleComposerOption(args: List<String>) {
    languageComposerOptions.getOrPut(LanguageComposerRegister[args[0]], ::mutableMapOf)[args[1]] = args[2]
  }

  fun openApp(args: List<String>) = openAppCallback(args)

  fun showKeyboardList(args: List<String>) = showKeyboardListCallback()

  class Callee(
      val functionName: String,
      _arguments: List<Argument>
  ) {
    val arguments = _arguments.toList()

    data class Argument(
        val type: Type,
        val value: String
    ) {
      enum class Type { RAW, MAP }
    }
  }
}