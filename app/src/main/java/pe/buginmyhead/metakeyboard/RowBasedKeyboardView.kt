package pe.buginmyhead.metakeyboard

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.net.Uri
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.os.Handler
import android.widget.Toast
import androidx.preference.PreferenceManager
import java.io.FileNotFoundException
import kotlin.math.roundToInt

class RowBasedKeyboardView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
  var metakeyboardService: MetakeyboardService? = null
  lateinit var keyboardModel: RowBasedKeyboard.Model

  private val fastClickTimer = Handler()
  private val fastClickRunnable = Runnable {
    keyboardModel.onTime(MetakeyboardLayout.ModelDelegate.TimerEvent.SLOW_CLICK)
  }

  private var repeatCount = 0
  private val longClickTimer = Handler()
  private val longClickRunnable = Runnable {
    repeatCount = 0
    repeat(MetakeyboardLayout.ModelDelegate.TimerEvent.LONG_CLICK)
  }
  private val repeatRunnable = Runnable {
    repeat(MetakeyboardLayout.ModelDelegate.TimerEvent.REPEAT)
  }

  private var backgroundImage: Bitmap? = null
  private val backgroundPaint = Paint().apply { style = Paint.Style.FILL_AND_STROKE; }
  private val buttonPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply { style = Paint.Style.FILL_AND_STROKE; }
  private val textPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply { style = Paint.Style.FILL_AND_STROKE; }

  override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
    val keyboardHeight =
        getSettingsInt("keyboard_height", R.integer.pref_keyboard_height_default)
    setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), keyboardHeight)
  }

  override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
    val viewWidth = right - left
    val viewHeight = bottom - top
    val viewRatio = viewWidth.toDouble() / viewHeight.toDouble()

    try {
      backgroundImage = null
      val uri = Uri.parse(getSettingsString("keyboard_background_image", ""))
      if (uri.toString().isNotEmpty()) {
        val source = BitmapFactory.decodeStream(context.contentResolver.openInputStream(uri))
        val sourceRatio = source.width.toDouble() / source.height.toDouble()
        val scaled =
            when (
              getSettingsString(
                  "keyboard_background_image_policy",
                  R.string.pref_keyboard_background_image_policy_default
              )
              ) {
              "stretch" -> {
                Bitmap.createScaledBitmap(source, viewWidth, viewHeight, true)
              }
              // crop(default) or else
              else -> {
                if (sourceRatio < viewRatio) Bitmap.createScaledBitmap(
                    source, viewWidth, (viewWidth / sourceRatio).roundToInt(), true
                ) else Bitmap.createScaledBitmap(
                    source, (viewHeight * sourceRatio).roundToInt(), viewHeight, true
                )
              }
            }
        backgroundImage = Bitmap.createBitmap(
            scaled,
            if (scaled.width <= viewWidth) 0 else (scaled.width - viewWidth) / 2,
            if (scaled.height <= viewHeight) 0 else (scaled.height - viewHeight) / 2,
            viewWidth,
            viewHeight
        )
      }
    } catch (exc: FileNotFoundException) {
      Toast.makeText(context, "파일이 삭제되었거나 훼손되었습니다. 이미지 파일을 다시 골라주세요", Toast.LENGTH_SHORT).show()
    } catch (exc: SecurityException) {
      Toast.makeText(context, "파일이 삭제되었거나 훼손되었습니다. 이미지 파일을 다시 골라주세요", Toast.LENGTH_SHORT).show()
    }

    try {
      val uri = Uri.parse(getSettingsString("load_keyboard_layout", ""))
      MetakeyboardApplication.keyboard = XmlInflater.rowBasedKeyboard(context.contentResolver.openInputStream(uri)!!)
    } catch (exc: FileNotFoundException) {
      MetakeyboardModel.showKeyboardListCallback()
//      if (PreferenceManager.getDefaultSharedPreferences(context).getString("load_keyboard_layout", "") == "")
//        Toast.makeText(context, "정상적인 앱 사용을 위해서는 레이아웃 파일을 설정해 주셔야 합니다", Toast.LENGTH_LONG).show()
//      else
//        Toast.makeText(context, "파일이 삭제되었거나 훼손되었습니다. 레이아웃 파일을 다시 골라주세요", Toast.LENGTH_LONG).show()
    } catch (exc: IllegalArgumentException) {
      MetakeyboardModel.showKeyboardListCallback()
    }
    keyboardModel = MetakeyboardApplication.keyboard.Model(viewWidth, viewHeight)
  }

  override fun onDraw(canvas: Canvas) {
    val alphaMask =
        if (getSettingsString("keyboard_background_image", "").isEmpty()) Color.BLACK
        else Color.TRANSPARENT
    backgroundPaint.color =
        getSettingsInt("keyboard_background_color", R.color.colorKeyboardBackground) or alphaMask
    buttonPaint.color =
        getSettingsInt("keyboard_button_color", R.color.colorKeyboardButton) or alphaMask
    textPaint.color =
        getSettingsInt("keyboard_text_color", R.color.colorKeyboardText) or alphaMask
    textPaint.textSize = 50F
    textPaint.textAlign = Paint.Align.CENTER
    val textHeight = textPaint.descent() - textPaint.ascent()
    val textOffset = textHeight / 2 - textPaint.descent()
    val gap = getSettingsInt("keyboard_gap_size", R.integer.pref_button_gap_default)

    backgroundImage?.let { canvas.drawBitmap(it, 0F, 0F, null) }

    if (/*keyboard.hasButton()*/true) {
      var lastRowBorderline = 0F
      keyboardModel.rowModelBorderlines.forEachIndexed { rowIndex, rmb ->
        canvas.drawRect(0F, lastRowBorderline, width.toFloat(), lastRowBorderline + gap, backgroundPaint)

        var lastButtonBorderline = 0F
        rmb.model.buttonBorderlines.forEachIndexed { buttonIndex, buttonBorderline ->
          canvas.drawRect(lastButtonBorderline, lastRowBorderline + gap, lastButtonBorderline + gap, rmb.borderline - gap, backgroundPaint)

          canvas.save()
          canvas.clipRect(lastButtonBorderline + gap, lastRowBorderline + gap, buttonBorderline - gap, rmb.borderline - gap)
          canvas.drawPaint(buttonPaint)
          MetakeyboardApplication.keyboard.rows[rowIndex].buttons[buttonIndex].imprints.forEach {
            val text = it.texts.fold("") { acc, text -> acc + text.text }
            canvas.drawText(text, (lastButtonBorderline + buttonBorderline) / 2, (lastRowBorderline + rmb.borderline) / 2 + textOffset, textPaint)
          }
          canvas.restore()

          canvas.drawRect(buttonBorderline - gap, lastRowBorderline + gap, buttonBorderline, rmb.borderline - gap, backgroundPaint)

          lastButtonBorderline = buttonBorderline
        }

        canvas.drawRect(0F, rmb.borderline - gap, width.toFloat(), rmb.borderline, backgroundPaint)
        lastRowBorderline = rmb.borderline
      }
    } else {
      canvas.drawColor(backgroundPaint.color)
    }
  }

  @SuppressLint("ClickableViewAccessibility")
  override fun onTouchEvent(event: MotionEvent): Boolean {
    MetakeyboardModel.integrationComposer.lackOfCommittedTextEventHandler = {
      val ret = metakeyboardService!!.currentInputConnection.getTextBeforeCursor(1, 0)
      metakeyboardService!!.currentInputConnection.deleteSurroundingText(1, 0)
      ret
    }

    when (event.action) {
      MotionEvent.ACTION_DOWN -> {
        longClickTimer.postDelayed(
            longClickRunnable, getSettingsString("long_click", R.string.pref_long_click_default).toLong()
        )
        keyboardModel.onTouch(event.x, event.y, MetakeyboardLayout.ModelDelegate.TouchAction.DOWN)
      }
      MotionEvent.ACTION_MOVE -> {
        keyboardModel.onTouch(event.x, event.y, MetakeyboardLayout.ModelDelegate.TouchAction.MOVE)
      }
      MotionEvent.ACTION_UP -> {
        longClickTimer.removeCallbacksAndMessages(null)
        fastClickTimer.removeCallbacksAndMessages(null)
        fastClickTimer.postDelayed(
            fastClickRunnable, getSettingsString("fast_click", R.string.pref_fast_click_default).toLong()
        )
        keyboardModel.onTouch(event.x, event.y, MetakeyboardLayout.ModelDelegate.TouchAction.UP)
        updateText()
      }
    }

    if (keyboardModel.popupModel == null) {

    } else {

    }

    return true
  }

  private fun updateText() {
    metakeyboardService!!.currentInputConnection.apply {
      commitText(MetakeyboardModel.integrationComposer.pullCommitted(), 1)
      setComposingText(MetakeyboardModel.integrationComposer.pullUncommitted(), 1)
    }
    MetakeyboardModel.integrationComposer.clearCommitted()
  }

  private fun repeat(timerEvent: MetakeyboardLayout.ModelDelegate.TimerEvent) {
    keyboardModel.onTime(timerEvent)
    keyboardModel.rate?.let {
      val repeaterRateFast =
          getSettingsString("repeater_rate_fast", R.string.pref_repeater_rate_fast_default).toLong()
      val repeaterRateSlow =
          getSettingsString("repeater_rate_slow", R.string.pref_repeater_rate_slow_default).toLong()
      val repeaterAccelerateCount =
          getSettingsString("repeater_accelerate_count", R.string.pref_repeater_accelerate_count_default).toInt()
      longClickTimer.postDelayed(
          repeatRunnable,
          when (it) {
            RowBasedKeyboard.AbstractButton.RepeaterButton.Rate.FAST -> repeaterRateFast
            RowBasedKeyboard.AbstractButton.RepeaterButton.Rate.SLOW -> repeaterRateSlow
            RowBasedKeyboard.AbstractButton.RepeaterButton.Rate.ACCELERATE ->
              if (repeatCount < repeaterAccelerateCount) {
                repeatCount++
                repeaterRateSlow
              } else {
                repeaterRateFast
              }
          }
      )
    }
    updateText()
  }

  private fun getSettingsString(key: String, defValId: Int) =
      PreferenceManager
          .getDefaultSharedPreferences(context)
          .getString(key, resources.getString(defValId))!!

  private fun getSettingsString(key: String, defVal: String) =
      PreferenceManager
          .getDefaultSharedPreferences(context)
          .getString(key, defVal)!!

  private fun getSettingsInt(key: String, defValId: Int) =
      PreferenceManager
          .getDefaultSharedPreferences(context)
          .getInt(key, resources.getInteger(defValId))
}