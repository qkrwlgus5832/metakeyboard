package pe.buginmyhead.metakeyboard

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

class KeyboardHeightControlBar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
  private val paint = Paint()

  override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
    setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), 100)
  }

  override fun onDraw(canvas: Canvas) {
    paint.color = Color.YELLOW
    canvas.drawPaint(paint)
  }
}