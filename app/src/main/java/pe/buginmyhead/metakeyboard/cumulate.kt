package pe.buginmyhead.metakeyboard

fun <T, R> Iterable<T>.cumulate(initial: R, operation: (acc: R, T) -> R) = sequence {
  var last = initial
  forEach {
    last = operation(last, it)
    yield(last)
  }
}