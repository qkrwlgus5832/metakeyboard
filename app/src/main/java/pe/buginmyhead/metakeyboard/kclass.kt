package pe.buginmyhead.metakeyboard

/**
 * This extension property exists for cases where imaginary syntax like
 * "something?::class" is needed. Use "something::class" instead when
 * 'something' is not null to avoid null safety call(?.) or non-null
 * assertion(!!).
 */
val Any?.kclass get() = this?.let { it::class }