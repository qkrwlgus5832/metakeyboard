package pe.buginmyhead.metakeyboard

import kotlin.reflect.KProperty

class NonNegative<T>(
    private var realValue: T
) where T : Number, T : Comparable<T> {
  operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
    return realValue
  }

  operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
    require(value >= 0 as T) { "${property.name} value must be non-negative, was $value" }
    realValue = value
  }
}