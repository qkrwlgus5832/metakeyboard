package pe.buginmyhead.metakeyboard

import java.util.*

abstract class MetakeyboardOfficialAbstractPrimitiveComposer : PrimitiveComposer {
  protected val stableComposees = LinkedList<PrimitiveComposer.Composee>()
  protected val uncommittedBuilder = StringBuilder()
  protected val committedBuilder = StringBuilder()
  private var isDirty = true

  final override fun pullCommitted(): CharSequence {
    needToCompose()
    return committedBuilder
  }

  final override fun pullUncommitted(): CharSequence {
    needToCompose()
    return uncommittedBuilder
  }

  override fun clearCommitted() {
    committedBuilder.clear()
  }

  protected fun needToCompose() {
    if (!isDirty) return
    compose()
    isDirty = false
  }

  protected abstract fun compose()

  internal fun makeDirty() {
    isDirty = true
  }
}