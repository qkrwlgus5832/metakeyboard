package pe.buginmyhead.metakeyboard

import android.content.SharedPreferences
import android.inputmethodservice.InputMethodService
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.preference.PreferenceManager

class MetakeyboardService : InputMethodService(),
    SharedPreferences.OnSharedPreferenceChangeListener {
  private lateinit var keyboardView: RowBasedKeyboardView

  override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
    MetakeyboardApplication.onSharedPreferenceChanged(key, keyboardView)
  }

  override fun onCreate() {
    super.onCreate()

    PreferenceManager
        .getDefaultSharedPreferences(this)
        .registerOnSharedPreferenceChangeListener(this)

    keyboardView = RowBasedKeyboardView(this).apply {
      metakeyboardService = this@MetakeyboardService
    }
  }

  override fun onDestroy() {
    PreferenceManager
        .getDefaultSharedPreferences(this)
        .unregisterOnSharedPreferenceChangeListener(this)

    super.onDestroy()
  }

  override fun onCreateInputView(): View {
    super.onCreateInputView()
    return keyboardView
  }

  override fun onStartInput(attribute: EditorInfo?, restarting: Boolean) {
    resetInputMethodEditor()
  }

  override fun onUpdateSelection(
      oldSelStart: Int, oldSelEnd: Int, newSelStart: Int, newSelEnd: Int,
      candidatesStart: Int, candidatesEnd: Int
  ) {
    if (oldSelStart == oldSelEnd && oldSelEnd == candidatesEnd) {
      currentInputConnection.finishComposingText()
      resetInputMethodEditor()
    }
    super.onUpdateSelection(oldSelStart, oldSelEnd, newSelStart, newSelEnd, candidatesStart, candidatesEnd)
  }

  private fun resetInputMethodEditor() {
    MetakeyboardModel.integrationComposer.clearCommitted()
    MetakeyboardModel.integrationComposer.clearUncommitted()
    try {
      keyboardView.keyboardModel.resetClickCounter()
    } catch (exc: UninitializedPropertyAccessException) {
    }
  }
}