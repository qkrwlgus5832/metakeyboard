package pe.buginmyhead.metakeyboard

abstract class MetakeyboardOfficialAbstractLanguageComposer :
  MetakeyboardOfficialAbstractPrimitiveComposer(),
  LanguageComposer {
  protected var consumedComposeeCounter = 0

  final override fun push(composee: PrimitiveComposer.Composee) {
    makeDirty()
    stableComposees.add(composee)
  }

  final override fun countConsumed(): Int {
    needToCompose()
    return consumedComposeeCounter
  }

  final override fun clearCommitted() {
    consumedComposeeCounter = 0
    super.clearCommitted()
  }

  final override fun clearUncommitted() {
    makeDirty()
    stableComposees.clear()
  }
}