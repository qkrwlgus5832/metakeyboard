package pe.buginmyhead.metakeyboard

// TODO: eliminate dependency to Java
import java.util.*

abstract class MetakeyboardOfficialAbstractIntegrationComposer :
  MetakeyboardOfficialAbstractPrimitiveComposer(),
  IntegrationComposer {
  protected val volatileComposees = LinkedList<PrimitiveComposer.Composee>()

  final override var lackOfCommittedTextEventHandler: () -> CharSequence = { "" }

  final override fun push(composee: PrimitiveComposer.Composee?, volatility: Boolean) {
    if (volatility) {
      composee?.let {
        makeDirty()
        volatileComposees.add(it)
      }
    } else {
      makeDirty()
      stableComposees.addAll(volatileComposees)
      volatileComposees.clear()
      composee?.let { stableComposees.add(it) }
    }
  }

  final override fun push(composee: PrimitiveComposer.Composee) {
    super.push(composee)
  }

  final override fun clearCommitted() {
    super.clearCommitted()
  }

  final override fun clearUncommitted() {
    clearVolatile()
    stableComposees.clear()
  }

  final override fun clearVolatile() {
    makeDirty()
    volatileComposees.clear()
  }
}