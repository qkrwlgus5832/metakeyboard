package pe.buginmyhead.metakeyboard

import kotlin.reflect.KClass

/**
 * This exists because Kotlin or Java Reflection can't search a class in a package.
 * LanguageComposer authors should add their LanguageComposer class in here.
 * Please add one in alphabetical order.
 */
object LanguageComposerRegister {
  operator fun get(index: String): KClass<out LanguageComposer> = when (index) {
    "Hangul" -> Hangul::class
    else -> throw NoSuchElementException()
  }
}