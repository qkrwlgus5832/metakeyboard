package pe.buginmyhead.metakeyboard

import android.Manifest
import android.os.Bundle
import android.content.Intent
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager

class SettingsActivity : AppCompatActivity(),
    PreferenceFragmentCompat.OnPreferenceStartFragmentCallback {
  companion object {
    private const val TITLE_TAG = "settings_activity_title"
    private const val KEYBOARD_LAYOUT_DIRECTORY_REQUEST_CODE = 1
    private const val KEYBOARD_BACKGROUND_IMAGE_REQUEST_CODE = 2

    private fun startStorageAccessFrameworkActivity(activity: Activity, requestCode: Int) {
      //intent.flags = Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION
      when (requestCode) {
        KEYBOARD_BACKGROUND_IMAGE_REQUEST_CODE -> {
          Intent(Intent.ACTION_OPEN_DOCUMENT).run {
            type = "image/*"
            activity.startActivityForResult(this, KEYBOARD_BACKGROUND_IMAGE_REQUEST_CODE)
          }
        }
        KEYBOARD_LAYOUT_DIRECTORY_REQUEST_CODE -> {
          Intent(Intent.ACTION_OPEN_DOCUMENT_TREE).run {
            activity.startActivityForResult(this, KEYBOARD_LAYOUT_DIRECTORY_REQUEST_CODE)
          }
        }
        else -> {
          throw IllegalArgumentException("Illegal requestCode")
        }
      }
    }

    private fun requestStorageAccessPermission(activity: Activity, requestCode: Int) {
      if (
          ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
          != PackageManager.PERMISSION_GRANTED
      ) {
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
            requestCode
        )
      } else {
        startStorageAccessFrameworkActivity(activity, requestCode)
      }
    }
  }

  private lateinit var editTextForTestingKeyboard: EditText
  private lateinit var alertDialogForTestingKeyboard: AlertDialog
  private val showSoftInput = Runnable {
    (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        .showSoftInput(editTextForTestingKeyboard, InputMethodManager.SHOW_IMPLICIT)
  }

  override fun onPreferenceStartFragment(caller: PreferenceFragmentCompat, pref: Preference): Boolean {
    // Instantiate the new Fragment
    val args = pref.extras
    val fragment =
        supportFragmentManager
            .fragmentFactory
            .instantiate(classLoader, pref.fragment, args)
            .apply {
              arguments = args
              setTargetFragment(caller, 0)
            }
    // Replace the existing Fragment with the new Fragment
    supportFragmentManager
        .beginTransaction()
        .replace(R.id.settings, fragment)
        .addToBackStack(null)
        .commit()
    title = pref.title
    return true
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_settings)
    if (savedInstanceState == null) {
      supportFragmentManager
          .beginTransaction()
          .replace(R.id.settings, SettingsHomeFragment())
          .commit()
      setTitle(R.string.title_activity_settings)
    } else {
      title = savedInstanceState.getCharSequence(TITLE_TAG)
    }
    supportFragmentManager.addOnBackStackChangedListener {
      if (supportFragmentManager.backStackEntryCount == 0) {
        setTitle(R.string.title_activity_settings)
      }
    }
    supportActionBar?.setDisplayHomeAsUpEnabled(true)

    editTextForTestingKeyboard = EditText(this).apply {
      hint = "Test your keyboard"
    }
    alertDialogForTestingKeyboard =
        AlertDialog.Builder(this)
            .setView(editTextForTestingKeyboard)
            .create()
  }

  override fun onResume() {
    // TODO: receive extra (now null is received)
    intent.extras?.run {
      when (this.getString("setting")) {
        "select_directory" -> requestStorageAccessPermission(this@SettingsActivity, KEYBOARD_LAYOUT_DIRECTORY_REQUEST_CODE)
      }
    }
    super.onResume()
  }

  override fun onSaveInstanceState(outState: Bundle) {
    super.onSaveInstanceState(outState)
    // Save current activity title so we can set it again after a configuration change
    outState.putCharSequence(TITLE_TAG, title)
  }

  override fun onSupportNavigateUp(): Boolean {
    return if (supportFragmentManager.popBackStackImmediate()) true
    else super.onSupportNavigateUp()
  }

  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    menuInflater.inflate(R.menu.settings_activity_action_buttons, menu)
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    return when (item?.itemId) {
      R.id.action_show_keyboard_test_dialog -> {
        alertDialogForTestingKeyboard.show()
        if (editTextForTestingKeyboard.requestFocus()) {
          editTextForTestingKeyboard.post(showSoftInput)
        }
        true
      }
      else -> {
        super.onOptionsItemSelected(item)
      }
    }
  }

  private fun showPermissionDialog() {
    AlertDialog.Builder(this)
        .setMessage("저장소 접근이 필요합니다. 휴대폰 설정 > 애플리케이션 관리 > 애플리케이션 관리자" +
            " > Metakeyboard에서 저장 공간 접근을 허용으로 바꿔주세요")
        .setPositiveButton("설정") { _, _ ->
          startActivity(Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS))
        }
        .show()
  }

  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
    when (requestCode) {
      KEYBOARD_LAYOUT_DIRECTORY_REQUEST_CODE, KEYBOARD_BACKGROUND_IMAGE_REQUEST_CODE -> {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          startStorageAccessFrameworkActivity(this, requestCode)
        } else if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
          if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            showPermissionDialog()
          } else {
            Toast.makeText(this, "동의 하셔야 합니다", Toast.LENGTH_SHORT).show()
          }
        }
      }
    }
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    if (resultCode == Activity.RESULT_OK && data != null) {
      val uri = data.data!!
      this.contentResolver.takePersistableUriPermission(
          uri, Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
      )
      PreferenceManager
          .getDefaultSharedPreferences(this)
          .edit()
          .putString(
              when(requestCode) {
                KEYBOARD_LAYOUT_DIRECTORY_REQUEST_CODE -> "load_keyboard_layout_directory"
                KEYBOARD_BACKGROUND_IMAGE_REQUEST_CODE -> "keyboard_background_image"
                else -> ""
          }, uri.toString())
          .apply()
    }
  }

  class SettingsHomeFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
      setPreferencesFromResource(R.xml.pref_settings_home, rootKey)
    }

    override fun onPreferenceTreeClick(preference: Preference?): Boolean {
      return when (preference?.key) {
        "keyboard_height" -> {
          MetakeyboardModel.adjustKeyboardBorderCallback()
          true
        }
        "load_keyboard_layout" -> {
          MetakeyboardModel.showKeyboardListCallback()
          true
        }
        "load_keyboard_layout_directory" -> {
          requestStorageAccessPermission(activity!!, KEYBOARD_LAYOUT_DIRECTORY_REQUEST_CODE)
          true
        }
        else -> {
          super.onPreferenceTreeClick(preference)
        }
      }
    }
  }

  class TimerPreferenceFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
      setPreferencesFromResource(R.xml.pref_timer, rootKey)
    }
  }

  class KeyboardLookAndFeelPreferenceFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
      setPreferencesFromResource(R.xml.pref_keyboard_look_and_feel, rootKey)
    }

    override fun onPreferenceTreeClick(preference: Preference?): Boolean {
      return when (preference?.key) {
        "keyboard_not_use_background_image" -> {
          PreferenceManager
              .getDefaultSharedPreferences(context)
              .edit()
              .remove("keyboard_background_image")
              .apply()
          true
        }
        "keyboard_background_image" -> {
          requestStorageAccessPermission(activity!!, KEYBOARD_BACKGROUND_IMAGE_REQUEST_CODE)
          true
        }
        else -> {
          super.onPreferenceTreeClick(preference)
        }
      }
    }
  }
}