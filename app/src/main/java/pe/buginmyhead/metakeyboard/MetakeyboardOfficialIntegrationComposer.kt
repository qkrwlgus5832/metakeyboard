package pe.buginmyhead.metakeyboard

class MetakeyboardOfficialIntegrationComposer :
  MetakeyboardOfficialAbstractIntegrationComposer() {
  private var currentLanguageComposer: LanguageComposer? = null

  // TODO: enhance algorithm
  override fun compose() {
    val stableComposeeListIterator = stableComposees.listIterator()

    fun flushStable() {
      currentLanguageComposer?.run {
        rewind(stableComposeeListIterator)
        repeat(countConsumed()) {
          stableComposeeListIterator.next()
        }
        migrate(pullCommitted(), stableComposeeListIterator)
        proceed(stableComposeeListIterator)
        uncommittedBuilder.append(pullUncommitted())
        clearCommitted()
      }
    }

    preprocessGlobalNamedElement(stableComposeeListIterator)
    uncommittedBuilder.clear()
    currentLanguageComposer?.run {
      clearCommitted()
      clearUncommitted()
    }
    while (stableComposeeListIterator.hasNext()) {
      val composee = stableComposeeListIterator.next()
      when (composee.category) {
        PrimitiveComposer.Category.NAMED_ELEMENT, PrimitiveComposer.Category.GRAPHEME -> {
          composeNamedElementOrGrapheme(composee, stableComposeeListIterator, ::flushStable)
        }
        PrimitiveComposer.Category.TEXT -> {
          stableComposeeListIterator.previous()
          flushStable()
          stableComposeeListIterator.next()
          currentLanguageComposer?.clearUncommitted()
          migrate(composee.value, stableComposeeListIterator)
        }
      }
    }

    val tempComposeeList = (stableComposees + volatileComposees).toMutableList()
    val tempComposeeListIterator = tempComposeeList.listIterator()

    fun flushTemp() {
      currentLanguageComposer?.run {
        uncommittedBuilder.run {
          append(pullCommitted())
          append(pullUncommitted())
        }
        clearCommitted()
      }
    }

    preprocessGlobalNamedElement(tempComposeeListIterator)
    // Not to build a composed string twice remaining composees in stableComposees
    uncommittedBuilder.clear()
    currentLanguageComposer?.run {
      clearCommitted()
      clearUncommitted()
    }
    while (tempComposeeListIterator.hasNext()) {
      val composee = tempComposeeListIterator.next()
      when (composee.category) {
        PrimitiveComposer.Category.NAMED_ELEMENT, PrimitiveComposer.Category.GRAPHEME -> {
          composeNamedElementOrGrapheme(composee, tempComposeeListIterator, ::flushTemp)
        }
        PrimitiveComposer.Category.TEXT -> {
          flushTemp()
          currentLanguageComposer?.clearUncommitted()
          uncommittedBuilder.append(composee.value)
        }
      }
    }
  }

  private fun migrate(charSequence: CharSequence, iter: MutableListIterator<PrimitiveComposer.Composee>) {
    committedBuilder.append(uncommittedBuilder)
    uncommittedBuilder.clear()
    committedBuilder.append(charSequence)
    while (iter.hasPrevious()) {
      iter.previous()
      iter.remove()
    }
  }

  // preprocess some global named elements (e.g. global_h_backspace)
  private fun preprocessGlobalNamedElement(iter: MutableListIterator<PrimitiveComposer.Composee>) {
    while (iter.hasNext()) {
      val composee = iter.next()
      if (
          composee.languageComposer == null
          && composee.category == PrimitiveComposer.Category.NAMED_ELEMENT
      ) when (composee.value) {
        "global_h_backspace" -> {
          iter.remove()
          if (iter.hasPrevious()) {
            // Following two results are same each other. See java.util.ListIterator
            iter.previous()
            val previous = iter.next()
            when (previous.category) {
              PrimitiveComposer.Category.GRAPHEME, PrimitiveComposer.Category.NAMED_ELEMENT -> {
                iter.remove()
              }
              PrimitiveComposer.Category.TEXT -> {
                iter.remove()
              }
            }
          } else {
            if (committedBuilder.isEmpty()) {
              committedBuilder.append(lackOfCommittedTextEventHandler())
            }
            if (committedBuilder.isNotEmpty()) {
              iter.add(PrimitiveComposer.Composee(PrimitiveComposer.Category.TEXT, committedBuilder.last().toString(), null))
              committedBuilder.deleteCharAt(committedBuilder.lastIndex)
              iter.previous()
              iter.remove()
            }
          }
        }
      }
    }

    // get the iterator back to start position
    while (iter.hasPrevious()) {
      iter.previous()
    }
  }

  private fun composeNamedElementOrGrapheme(
    composee: PrimitiveComposer.Composee,
    iter: ListIterator<PrimitiveComposer.Composee>,
    onLocaleComposerChanging: () -> Unit = {}
  ) {
    if (composee.languageComposer == currentLanguageComposer.kclass) {
      if (composee.languageComposer == null) {
        when (composee.category) {
          PrimitiveComposer.Category.NAMED_ELEMENT ->
            TODO("not implemented yet")
          PrimitiveComposer.Category.GRAPHEME ->
            throw UnsupportedOperationException("There is no grapheme for this integration composer")
          else ->
            throw IllegalArgumentException("Please look at the name of this method if you don't know why")
        }
      } else {
        currentLanguageComposer?.push(composee)
      }
    } else {
      iter.previous()
      onLocaleComposerChanging()
      iter.next()
      currentLanguageComposer =
          composee.languageComposer?.constructors?.first { it.parameters.isEmpty() }?.call()?.apply {
            push(composee)
          }
    }
    if (!iter.hasNext()) {
      onLocaleComposerChanging()
    }
  }

  private fun rewind(iter: MutableListIterator<PrimitiveComposer.Composee>, consumer: (PrimitiveComposer.Composee) -> Unit = {}) {
    while (iter.hasPrevious()) {
      val composee = iter.previous()
      if (composee.languageComposer == currentLanguageComposer.kclass) {
        consumer(composee)
      } else {
        iter.next()
        break
      }
    }
  }

  private fun proceed(iter: MutableListIterator<PrimitiveComposer.Composee>, consumer: (PrimitiveComposer.Composee) -> Unit = {}) {
    while (iter.hasNext()) {
      val composee = iter.next()
      if (composee.languageComposer == currentLanguageComposer.kclass) {
        consumer(composee)
      } else {
        iter.previous()
        break
      }
    }
  }
}