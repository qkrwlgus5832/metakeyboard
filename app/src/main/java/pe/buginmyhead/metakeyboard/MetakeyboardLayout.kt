package pe.buginmyhead.metakeyboard

import kotlin.reflect.KClass

class MetakeyboardLayout(
    val initialSetting: Actable,
    //val onSwitch = Action(),
    _keyboards: List<Keyboard>
) {
  val keyboards = _keyboards.toList()

  interface Callable {
    fun call()
  }

  interface Actable {
    fun act()
  }

  open class Action(
      _callables: List<Callable>
  ) : Actable {
    val callables = _callables.toList()

    override fun act() {
      callables.forEach { it.call() }
    }

    open class Builder {
      var callables = mutableListOf<MetakeyboardLayout.Callable>()

      open fun build() = Action(callables)

      fun callable(c: Callable) = callables.add(c)
    }
  }

  class Callee(
      val callee: MetakeyboardModel.Callee
  ) : Callable {
    constructor(
        functionName: String,
        arguments: List<MetakeyboardModel.Callee.Argument>
    ) : this(MetakeyboardModel.Callee(functionName, arguments))

    override fun call() {
      MetakeyboardModel.call(callee)
    }
  }

  data class Composee(
      val composerComposee: PrimitiveComposer.Composee
  ) : Callable {
    constructor(
      category: PrimitiveComposer.Category,
      value: String,
      languageComposer: KClass<out LanguageComposer>?,
      print: String? = null
    ) : this(PrimitiveComposer.Composee(category, value, languageComposer, print))

    override fun call() {
      MetakeyboardModel.integrationComposer.push(composerComposee, true)
    }
  }

  interface ModelDelegate {
    fun onTouch(x: Float, y: Float, touchAction: TouchAction)
    fun onTime(timerEvent: TimerEvent)

    enum class TouchAction { DOWN, MOVE, UP }
    enum class TimerEvent { LONG_CLICK, SLOW_CLICK, REPEAT }
  }

  abstract class Keyboard(
      val initialSetting: Actable
  ) {
//    fun getModelDelegate(width: Int, height: Int): ModelDelegate {
//      require(width >= 0) { "Width must be non-negative, was $width" }
//      require(height >= 0) { "Height must be non-negative, was $height" }
//      return makeModelDelegate(width, height)
//    }
//
//    protected abstract fun makeModelDelegate(width: Int, height: Int): ModelDelegate
  }

  abstract class Button
}