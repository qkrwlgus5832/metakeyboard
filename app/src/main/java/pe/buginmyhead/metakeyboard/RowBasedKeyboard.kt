package pe.buginmyhead.metakeyboard

fun rowBasedKeyboard(init: RowBasedKeyboard.Builder.() -> Unit) =
  RowBasedKeyboard.Builder().also(init).build()

class RowBasedKeyboard(
    val orientation: Orientation,
    initialSetting: MetakeyboardLayout.Actable,
    _rows: List<KeyboardRow>
) : MetakeyboardLayout.Keyboard(initialSetting) {
  class Builder {
    var orientation: Orientation = Orientation.HORIZONTAL
    var initialSetting: MetakeyboardLayout.Actable = MetakeyboardLayout.Action(listOf())
    var rows = mutableListOf<KeyboardRow>()

    fun build() = RowBasedKeyboard(orientation, initialSetting, rows)

    fun keyboardRow(init: KeyboardRow.Builder.() -> Unit) =
        rows.add(KeyboardRow.Builder().also(init).build())
  }

  val rows = _rows.toList()

  interface Reactable : MetakeyboardLayout.Actable {
    fun imprint()
  }

  class ButtonText(
      val texts: List<Text>,
      val firstPadding: Int = 0,
      val secondPadding: Int = 0,
      val thirdPadding: Int = 0,
      val fourthPadding: Int = 0,
      val lineGravity: Float = 0F,
      val textGravity: Float = 0F,
      val lineOffset: Int = 0,
      val textOffset: Int = 0,
      val lineAlignment: Float = 0F,
      val textAlignment: Float = 0F
  ) {
    class Text(
        val text: String,
        _fontSize: Float = 1F
    ) {
      val fontSize by NonNegative(_fontSize)

      class Builder {
        var text: String = ""
        var fontSize = 1F

        fun build() = Text(text, fontSize)
      }
    }

    class Builder {
      var texts = mutableListOf<Text>()
      var firstPadding: Int = 0
      var secondPadding: Int = 0
      var thirdPadding: Int = 0
      var fourthPadding: Int = 0
      var lineGravity: Float = 0F
      var textGravity: Float = 0F
      var lineOffset: Int = 0
      var textOffset: Int = 0
      var lineAlignment: Float = 0F
      var textAlignment: Float = 0F

      fun build() = ButtonText(
          texts,
          firstPadding, secondPadding, thirdPadding, fourthPadding,
          lineGravity, textGravity,
          lineOffset, textOffset,
          lineAlignment, textAlignment
      )

      fun text(init: Text.Builder.() -> Unit) =
          texts.add(ButtonText.Text.Builder().also(init).build())
    }
  }

  class Reaction(
      imprints: List<ButtonText>,
      callables: List<MetakeyboardLayout.Callable>
  ) : MetakeyboardLayout.Action(callables), Reactable {
    override fun imprint() {
      TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    class Builder : MetakeyboardLayout.Action.Builder() {
      var imprints = mutableListOf<ButtonText>()

      override fun build() = Reaction(imprints, callables)

      fun buttonText(init: ButtonText.Builder.() -> Unit) =
          imprints.add(ButtonText.Builder().also(init).build())
    }
  }

  interface Weighable {
    val weight: Float
  }

  sealed class AbstractButton(
      val style: Style,
      _weight: Float,
      val imprints: List<ButtonText>
  ) : MetakeyboardLayout.Button(), Weighable {
    final override val weight by NonNegative(_weight)

    enum class Style { EMPTY_SPACE, NORMAL }

    abstract class Builder {
      var style = Style.NORMAL
      var weight = 1F
      var imprints = mutableListOf<ButtonText>()

      protected fun buildReaction(init: Reaction.Builder.() -> Unit) =
          Reaction.Builder().also(init).build()

      fun buttonText(init: ButtonText.Builder.() -> Unit) =
          imprints.add(ButtonText.Builder().also(init).build())
    }

    class BaseButton(
        style: Style,
        weight: Float,
        imprints: List<ButtonText>,
        val reactable: Reactable
    ) : AbstractButton(style, weight, imprints), MetakeyboardLayout.Actable, PopupRow.Popable {
      override fun act() {
        reactable.act()
      }

      class Builder : AbstractButton.Builder() {
        var reactable: Reactable = Reaction(listOf(), listOf())

        fun build() = BaseButton(style, weight, imprints, reactable)

        fun reaction(init: Reaction.Builder.() -> Unit) {
          reactable = buildReaction(init)
        }
      }
    }

    sealed class AbstractSwitcherButton(
        style: Style,
        weight: Float,
        imprints: List<ButtonText>,
        _shortClicks: List<Reactable>,
        val hasTimeLimit: Boolean = false,
        val isOneWay: Boolean = false
    ) : AbstractButton(style, weight, imprints) {
      val shortClicks = _shortClicks.toList()

      open class Builder : AbstractButton.Builder() {
        var shortClicks = mutableListOf<Reactable>()
        var hasTimeLimit = false
        var isOneWay = false
      }

      class CustomLongClickButton(
          style: Style,
          weight: Float,
          imprints: List<ButtonText>,
          val longClick: Reactable?,
          shortClicks: List<Reactable>,
          hasTimeLimit: Boolean = false,
          isOneWay: Boolean = false
      ) : AbstractSwitcherButton(style, weight, imprints, shortClicks, hasTimeLimit, isOneWay), PopupRow.Popable {
        class Builder : AbstractSwitcherButton.Builder() {
          var longClick: Reactable? = null

          fun build() = CustomLongClickButton(style, weight, imprints, longClick, shortClicks, hasTimeLimit, isOneWay)

          fun longClickReaction(init: Reaction.Builder.() -> Unit) {
            longClick = buildReaction(init)
          }

          fun shortClickReaction(init: Reaction.Builder.() -> Unit) =
              shortClicks.add(buildReaction(init))
        }
      }

      class PopupButton(
          style: Style,
          weight: Float,
          imprints: List<ButtonText>,
          val firstSection: PopupSection,
          val secondSection: PopupSection,
          val thirdSection: PopupSection,
          val fourthSection: PopupSection,
          shortClicks: List<Reactable>,
          hasTimeLimit: Boolean = false,
          isOneWay: Boolean = false
      ) : AbstractSwitcherButton(style, weight, imprints, shortClicks, hasTimeLimit, isOneWay) {
        class Builder : AbstractSwitcherButton.Builder() {
          var firstSection = PopupSection(listOf())
          var secondSection = PopupSection(listOf())
          var thirdSection = PopupSection(listOf())
          var fourthSection = PopupSection(listOf())

          fun build() = PopupButton(
              style, weight, imprints,
              firstSection, secondSection, thirdSection, fourthSection,
              shortClicks, hasTimeLimit, isOneWay
          )

          fun reaction(init: Reaction.Builder.() -> Unit) =
              shortClicks.add(buildReaction(init))
        }
      }
    }

    class RepeaterButton(
        style: Style,
        weight: Float,
        imprints: List<ButtonText>,
        val longClick: Reactable?,
        val shortClick: Reactable,
        val rate: Rate = Rate.FAST
    ) : AbstractButton(style, weight, imprints) {
      enum class Rate {
        FAST, SLOW, ACCELERATE
      }

      class Builder : AbstractButton.Builder() {
        var longClick: Reactable? = null
        var shortClick: Reactable = Reaction(listOf(), listOf())
        var rate: Rate = Rate.FAST

        fun build() = RepeaterButton(style, weight, imprints, longClick, shortClick, rate)

        fun longClickReaction(init: Reaction.Builder.() -> Unit) {
          longClick = buildReaction(init)
        }

        fun shortClickReaction(init: Reaction.Builder.() -> Unit) {
          shortClick = buildReaction(init)
        }
      }
    }
  }

  class PopupRow<T>(
      _weight: Float,
      buttons: List<T>
  ) : KeyboardRow(_weight, buttons) where T : AbstractButton, T : PopupRow.Popable {
    interface Popable
  }

  data class PopupSection(val rows: List<PopupRow<*>>)

  open class KeyboardRow(
      _weight: Float,
      _buttons: List<AbstractButton>
  ) : Weighable {
    final override val weight by NonNegative(_weight)
    val buttons = _buttons.toList()

    fun sumButtonWeight() = buttons.map { it.weight }.sum()

    class Builder {
      var weight = 1F
      var buttons = mutableListOf<AbstractButton>()

      fun build() = KeyboardRow(weight, buttons)

      fun customLongClickButton(
          init: AbstractButton.AbstractSwitcherButton.CustomLongClickButton.Builder.() -> Unit
      ) =
          buttons.add(AbstractButton.AbstractSwitcherButton.CustomLongClickButton.Builder().also(init).build())

      fun baseButton(init: AbstractButton.BaseButton.Builder.() -> Unit) =
          buttons.add(AbstractButton.BaseButton.Builder().also(init).build())

      fun repeaterButton(init: AbstractButton.RepeaterButton.Builder.() -> Unit) =
          buttons.add(AbstractButton.RepeaterButton.Builder().also(init).build())
    }

    inner class Model(
        _width: Int
    ) {
      val width by NonNegative(_width)
      val totalButtonWeight = sumButtonWeight()

      val buttonBorderlines =
          buttons.cumulate(0F) { acc, button -> acc + button.weight * width / totalButtonWeight }.toList()
    }
  }

  enum class Orientation {
    HORIZONTAL, VERTICAL
  }

  fun sumRowWeight() = rows.map { it.weight }.sum()

  data class ButtonRect(val left: Float, val top: Float, val right: Float, val bottom: Float, val button: AbstractButton)

  data class RowModelBorderline(val model: KeyboardRow.Model, val borderline: Float)

  inner class PopupModel private constructor(
      _width: Int,
      _height: Int,
      popupButton: AbstractButton.AbstractSwitcherButton.PopupButton
  ) {
    val width by NonNegative(_width)
    val height by NonNegative(_height)
    val totalRowWeight = sumRowWeight()

    private val rowBorderlines =
        popupButton.firstSection.rows.cumulate(0F) { acc, row -> acc + row.weight * height / totalRowWeight }.toList()
    private val rowModels =
        popupButton.firstSection.rows.map { it.Model(width) }
    val rowModelBorderlines =
        rowModels.zip(rowBorderlines) { rowModel, borderline -> RowModelBorderline(rowModel, borderline) }
  }

  inner class Model(
      _width: Int,
      _height: Int
  ) : MetakeyboardLayout.ModelDelegate {
    val width by NonNegative(_width)
    val height by NonNegative(_height)
    val totalRowWeight = sumRowWeight()

    var rate: AbstractButton.RepeaterButton.Rate? = null
      private set
      get() = (lastDownButton as? AbstractButton.RepeaterButton)?.rate

    private val rowBorderlines =
        rows.cumulate(0F) { acc, row -> acc + row.weight * height / totalRowWeight }.toList()
    private val rowModels =
        rows.map { it.Model(width) }
    val rowModelBorderlines =
        rowModels.zip(rowBorderlines) { rowModel, borderline -> RowModelBorderline(rowModel, borderline) }

    private var clickCounter = 0
    private var isLongClicked = false
    private var isFastClicked = false
    private var lastDownButton: AbstractButton? = null
    private var lastUpButton: AbstractButton? = null

    var popupModel: PopupModel? = null

    fun resetClickCounter() {
      clickCounter = 0
    }

    override fun onTouch(
        x: Float, y: Float, touchAction: MetakeyboardLayout.ModelDelegate.TouchAction
    ) {
      fun notifyClickedButtonChanged() {
        isFastClicked = false
        MetakeyboardModel.integrationComposer.push()
      }

      val rowIndex =
          rowBorderlines.indexOfFirst { it > y }
      val buttonIndex =
          rowModelBorderlines.elementAtOrNull(rowIndex)?.model?.buttonBorderlines?.indexOfFirst { it > x } ?: return
      // if user touches an empty keyboard or row, return@onTouch
      val currentButton =
          rows.elementAtOrNull(rowIndex)?.buttons?.elementAtOrNull(buttonIndex) ?: return

      when (touchAction) {
        MetakeyboardLayout.ModelDelegate.TouchAction.DOWN -> {
          lastDownButton = currentButton
        }
        MetakeyboardLayout.ModelDelegate.TouchAction.MOVE -> {
          when (currentButton) {
            is AbstractButton.AbstractSwitcherButton.CustomLongClickButton -> {
              popupModel
            }
          }
        }
        MetakeyboardLayout.ModelDelegate.TouchAction.UP -> {
          when (currentButton) {
            is AbstractButton.BaseButton -> {
              if (lastDownButton === currentButton) {
                currentButton.reactable.act()
                notifyClickedButtonChanged()
              }
            }
            is AbstractButton.RepeaterButton -> {
              if (lastDownButton === currentButton && !isLongClicked) {
                currentButton.shortClick.act()
                notifyClickedButtonChanged()
              }
            }
            is AbstractButton.AbstractSwitcherButton.CustomLongClickButton -> {
              if (lastDownButton === currentButton) {
                if (!isLongClicked || currentButton.longClick == null) {
                  if (
                      lastUpButton !== currentButton
                      || currentButton.hasTimeLimit && !isFastClicked
                      || currentButton.shortClicks.size == 1
                  ) {
                    clickCounter = 0
                    notifyClickedButtonChanged()
                  }
                  if (currentButton.shortClicks.isNotEmpty()) {
                    MetakeyboardModel.integrationComposer.clearVolatile()
                    currentButton.shortClicks[clickCounter].act()
                    clickCounter = (clickCounter + 1) % currentButton.shortClicks.size
                  }
                }
              }
            }
            is AbstractButton.AbstractSwitcherButton.PopupButton -> {

              popupModel = null
              notifyClickedButtonChanged()
            }
          }
          isLongClicked = false
          if (lastDownButton === currentButton) {
            lastUpButton = currentButton
          }
          isFastClicked = true
        }
      }
    }

    override fun onTime(timerEvent: MetakeyboardLayout.ModelDelegate.TimerEvent) {
      lastDownButton ?: return

      val repeaterButtonAct = { repeaterButton: AbstractButton.RepeaterButton ->
        repeaterButton.longClick?.act() ?: repeaterButton.shortClick.act()
        MetakeyboardModel.integrationComposer.push()
      }

      // val for smart casting
      val fetchedLastDownButton = lastDownButton
      when (timerEvent) {
        MetakeyboardLayout.ModelDelegate.TimerEvent.LONG_CLICK -> {
          isLongClicked = true
          MetakeyboardModel.integrationComposer.push()
          when (fetchedLastDownButton) {
            is AbstractButton.BaseButton -> {
            }
            is AbstractButton.RepeaterButton -> {
              repeaterButtonAct(fetchedLastDownButton)
            }
            is AbstractButton.AbstractSwitcherButton.CustomLongClickButton -> {
              fetchedLastDownButton.longClick?.act()
            }
            is AbstractButton.AbstractSwitcherButton.PopupButton -> {
              popupModel
            }
          }
        }
        MetakeyboardLayout.ModelDelegate.TimerEvent.SLOW_CLICK -> {
          isFastClicked = false
        }
        MetakeyboardLayout.ModelDelegate.TimerEvent.REPEAT -> {
          when (fetchedLastDownButton) {
            is AbstractButton.RepeaterButton -> repeaterButtonAct(fetchedLastDownButton)
            else -> {}
          }
        }
      }
    }
  }
}