package pe.buginmyhead.metakeyboard

/**
 * @property FINAL_CONSONANT_COUNT includes case when there is no final CONSONANT
 */
class Hangul : MetakeyboardOfficialAbstractLanguageComposer() {
  companion object {
    const val BASE_CODE = '가'.toInt() // '\\uac00'
    //const val INITIAL_CONSONANT_COUNT = 19
    const val VOWEL_COUNT = 21
    const val FINAL_CONSONANT_COUNT = 28
    const val CONSONANT = 0
    const val VOWEL = 1
    const val NAMED_ELEMENT = 2
    const val NOT_ADEQUATE_GRAPHEME = -1
  }

  var currentChosung = '\u0000'
  var currentJungsung = '\u0000'
  var currentJongsung = '\u0000'
  var currentCheon = '\u0000'

  private fun commitCompleteChar(chosung: Char = '\u0000', jungsung: Char = '\u0000', jongsung: Char = '\u0000'): String {
    if (jungsung.toInt() == 0 && jongsung.toInt() == 0) {
      return ('ㄱ'.toInt() + if (chosung == '\u0000') 0 else chosung - 'ㄱ').toChar().toString()
    } else if (chosung.toInt() == 0 && jongsung.toInt() == 0)
      return ('ㅏ'.toInt() + if (jungsung == '\u0000') 0 else jungsung - 'ㅏ').toChar().toString()
    val completeChar =
      (getChosungIndex(chosung) * VOWEL_COUNT * FINAL_CONSONANT_COUNT + getJungsungIndex(jungsung) * FINAL_CONSONANT_COUNT + getJongsungIndex(jongsung) + BASE_CODE)

    return completeChar.toChar().toString()
  }

  private fun uncommitCompleteChar(chosung: Char = '\u0000', jungsung: Char = '\u0000', jongsung: Char = '\u0000'): String {
    currentChosung = chosung
    currentJungsung = jungsung
    currentJongsung = jongsung
    if (jungsung.toInt() == 0 && jongsung.toInt() == 0) {
      return ('ㄱ'.toInt() + if (chosung == '\u0000') 0 else chosung - 'ㄱ').toChar().toString()
    } else if (chosung.toInt() == 0 && jongsung.toInt() == 0)
      return ('ㅏ'.toInt() + if (jungsung == '\u0000') 0 else jungsung - 'ㅏ').toChar().toString()
    val completeChar =
      (getChosungIndex(chosung) * VOWEL_COUNT * FINAL_CONSONANT_COUNT + getJungsungIndex(jungsung) * FINAL_CONSONANT_COUNT + getJongsungIndex(jongsung) + BASE_CODE)

    return completeChar.toChar().toString()
  }

  private fun getChosungIndex(chosung: Char): Int {
    return when (chosung) {
      'ㄱ' -> 0; 'ㄲ' -> 1; 'ㄴ' -> 2; 'ㄷ' -> 3
      'ㄸ' -> 4; 'ㄹ' -> 5; 'ㅁ' -> 6; 'ㅂ' -> 7
      'ㅃ' -> 8; 'ㅅ' -> 9; 'ㅆ' -> 10; 'ㅇ' -> 11
      'ㅈ' -> 12; 'ㅉ' -> 13; 'ㅊ' -> 14; 'ㅋ' -> 15
      'ㅌ' -> 16; 'ㅍ' -> 17; 'ㅎ' -> 18
      else -> NOT_ADEQUATE_GRAPHEME
    }
  }

  private fun getJungsungIndex(jungsung: Char): Int {
    return when (jungsung) {
      'ㅏ' -> 0; 'ㅐ' -> 1; 'ㅑ' -> 2; 'ㅒ' -> 3
      'ㅓ' -> 4; 'ㅔ' -> 5; 'ㅕ' -> 6; 'ㅖ' -> 7
      'ㅗ' -> 8; 'ㅘ' -> 9; 'ㅙ' -> 10; 'ㅚ' -> 11
      'ㅛ' -> 12; 'ㅜ' -> 13; 'ㅝ' -> 14; 'ㅞ' -> 15
      'ㅟ' -> 16; 'ㅠ' -> 17; 'ㅡ' -> 18; 'ㅢ' -> 19
      'ㅣ' -> 20
      else -> NOT_ADEQUATE_GRAPHEME
    }
  }

  private fun getJongsungIndex(jongsung: Char): Int {
    return when (jongsung) {
      '\u0000' -> 0; 'ㄱ' -> 1; 'ㄲ' -> 2; 'ㄳ' -> 3
      'ㄴ' -> 4; 'ㄵ' -> 5; 'ㄶ' -> 6; 'ㄷ' -> 7
      'ㄹ' -> 8; 'ㄺ' -> 9; 'ㄻ' -> 10; 'ㄼ' -> 11
      'ㄽ' -> 12; 'ㄾ' -> 13; 'ㄿ' -> 14; 'ㅀ' -> 15
      'ㅁ' -> 16; 'ㅂ' -> 17; 'ㅄ' -> 18; 'ㅅ' -> 19
      'ㅆ' -> 20; 'ㅇ' -> 21; 'ㅈ' -> 22; 'ㅊ' -> 23
      'ㅋ' -> 24; 'ㅌ' -> 25; 'ㅍ' -> 26; 'ㅎ' -> 27
      else -> NOT_ADEQUATE_GRAPHEME
    }
  }

  private fun getOverlapConsonantorVowel(chosung: Char): Char {
    return when (chosung) {
      'ㄱ' -> 'ㅋ'; 'ㅋ' -> 'ㄱ'; 'ㄴ' -> 'ㄷ'; 'ㄷ' -> 'ㅌ'
      'ㅌ' -> 'ㄴ'; 'ㅁ' -> 'ㅂ'; 'ㅂ' -> 'ㅍ'; 'ㅍ' -> 'ㅁ'
      'ㅅ' -> 'ㅈ'; 'ㅈ' -> 'ㅊ'; 'ㅊ' -> 'ㅅ'; 'ㅇ' -> 'ㅎ'
      'ㅎ' -> 'ㅇ'; 'ㅏ' -> 'ㅑ'; 'ㅑ' -> 'ㅏ'; 'ㅓ' -> 'ㅕ'
      'ㅕ' -> 'ㅓ'; 'ㅗ' -> 'ㅛ'; 'ㅛ' -> 'ㅗ'; 'ㅜ' -> 'ㅠ'
      'ㅠ' -> 'ㅜ';
      else -> '0'
    }
  }

  private fun getDoubleConsonant(chosung: Char): Char {
    return when (chosung) {
      'ㄱ' -> 'ㄲ'; 'ㄲ' -> 'ㄱ'; 'ㄷ' -> 'ㄸ'; 'ㄸ' -> 'ㄷ'
      'ㅂ' -> 'ㅃ'; 'ㅃ' -> 'ㅂ'; 'ㅈ' -> 'ㅉ'; 'ㅉ' -> 'ㅈ'
      'ㅅ' -> 'ㅆ'; 'ㅆ' -> 'ㅅ'
      else -> '0'
    }
  }

  private fun moDictionary(lastMo: Char, currentMo: Char): Char {
    when (lastMo) {
      '·' -> {
        when (currentMo) {
          'ㅣ' -> {
            return 'ㅓ'
          }
          'ㅡ' -> {
            return 'ㅗ'
          }
        }
      }
      '‥' -> {
        when (currentMo) {
          'ㅣ' -> {
            return 'ㅕ'
          }
          'ㅡ' -> {
            return 'ㅛ'
          }
        }
      }
      'ㅏ' -> {
        when (currentMo) {
          'ㅣ' -> {
            return 'ㅐ'
          }
          '·' -> {
            return 'ㅑ'
          }
        }
      }
      'ㅑ' -> {
        when (currentMo) {
          'ㅣ' -> {
            return 'ㅒ'
          }
          '·' -> {
            return 'ㅏ'
          }
        }
      }
      'ㅓ' -> {
        when (currentMo) {
          'ㅣ' -> {
            return 'ㅔ'
          }
        }
      }
      'ㅕ' -> {
        when (currentMo) {
          'ㅣ' -> {
            return 'ㅖ'
          }
        }
      }
      'ㅗ' -> {
        when (currentMo) {
          'ㅣ' -> {
            return 'ㅚ'
          }
          'ㅏ' -> {
            return 'ㅘ'
          }
          'ㅐ' -> {
            return 'ㅙ'
          }
        }
      }
      'ㅚ' -> {
        when (currentMo) {
          '·' -> {
            return 'ㅘ'
          }
        }
      }
      'ㅜ' -> {
        when (currentMo) {
          'ㅣ' -> {
            return 'ㅟ'
          }
          'ㅓ' -> {
            return 'ㅝ'
          }
          'ㅔ' -> {
            return 'ㅞ'
          }
          '·' -> {
            return 'ㅠ'
          }
        }
      }
      'ㅠ' -> {
        when (currentMo) {
          'ㅣ' -> {
            return 'ㅝ'
          }
          '·' -> {
            return 'ㅜ'
          }
        }
      }
      'ㅝ' -> {
        when (currentMo) {
          'ㅣ' -> {
            return 'ㅞ'
          }
        }
      }
      'ㅘ' -> {
        when (currentMo) {
          'ㅣ' -> {
            return 'ㅙ'
          }
        }
      }
      'ㅡ' -> {
        when (currentMo) {
          'ㅣ' -> {
            return 'ㅢ'
          }
          '·' -> {
            return 'ㅜ'
          }
          '‥' -> {
            return 'ㅠ'
          }
        }
      }
      'ㅣ' -> {
        when (currentMo) {
          '·' -> {
            return 'ㅏ'
          }
          '‥' -> {
            return 'ㅑ'
          }
        }
      }
    }

    return '0'
  }

  private fun getFirstJa(InputJa: Char): Char {
    when (InputJa) {
      'ㄳ' -> return 'ㄱ'
      'ㄵ', 'ㄶ' -> return 'ㄴ'
      'ㄺ', 'ㄻ', 'ㄼ', 'ㄽ', 'ㄾ', 'ㄿ', 'ㅀ' -> return 'ㄹ'
      'ㅄ' -> return 'ㅂ'
    }
    return '0'
  }

  private fun getSecondJa(InputJa: Char): Char {
    when (InputJa) {
      'ㄳ' -> return 'ㅅ'
      'ㄵ' -> return 'ㅈ'
      'ㄶ' -> return 'ㅎ'
      'ㄺ' -> return 'ㄱ'
      'ㄻ' -> return 'ㅁ'
      'ㄼ' -> return 'ㅂ'
      'ㄽ' -> return 'ㅅ'
      'ㄾ' -> return 'ㅌ'
      'ㄿ' -> return 'ㅍ'
      'ㅀ' -> return 'ㅎ'
      'ㅄ' -> return 'ㅅ'
    }
    return '0'
  }

  private fun jaDictionary(lastJa: Char, currentJa: Char): Char {
    when (lastJa) {
      'ㄱ' -> {
        when (currentJa) {
          'ㅅ' -> {
            return 'ㄳ'
          }
        }
      }
      'ㄴ' -> {
        when (currentJa) {
          'ㅈ' -> {
            return 'ㄵ'
          }
          'ㅎ' -> {
            return 'ㄶ'
          }
        }
      }
      'ㄹ' -> {
        when (currentJa) {
          'ㄱ' -> {
            return 'ㄺ'
          }
          'ㅁ' -> {
            return 'ㄻ'
          }
          'ㅂ' -> {
            return 'ㄼ'
          }
          'ㅅ' -> {
            return 'ㄽ'
          }
          'ㅌ' -> {
            return 'ㄾ'
          }
          'ㅍ' -> {
            return 'ㄿ'
          }
          'ㅎ' -> {
            return 'ㅀ'
          }
        }
      }
      'ㅂ' -> {
        when (currentJa) {
          'ㅅ' -> {
            return 'ㅄ'
          }
        }
      }
    }

    return '0'
  }

  private fun getStableComposeesValue(index: Int): Char {
    return stableComposees[index].value[0]
  }

  override fun compose() {
    var state = "O"

    currentChosung = '\u0000'
    currentJungsung = '\u0000'
    currentJongsung = '\u0000'
    currentCheon = '\u0000'

    var namedElementConsumedCount = 0

    consumedComposeeCounter = 0
    committedBuilder.clear()
    uncommittedBuilder.clear()

    loop@ for (i in 0 until stableComposees.size) {
      uncommittedBuilder.clear()

      val consonantOrVowel = when (stableComposees[i].value) {
        in "ㄱ".."ㅎ" -> {
          if (currentCheon == '·' || currentCheon == '‥') {
            when (state) {
              "J", "JJ" -> {
                committedBuilder.append(commitCompleteChar(currentChosung))
              }
              "JMJ", "JMMJ", "JMMMJ" -> {
                committedBuilder.append(commitCompleteChar(currentChosung, currentJungsung))
              }
              "JMJJ", "JMMJJ", "JMMMJJ" -> {
                committedBuilder.append(commitCompleteChar(currentChosung, currentJongsung))
              }
            }
            committedBuilder.append(currentCheon)
            consumedComposeeCounter += state.length + 1 + namedElementConsumedCount
            state = "O"
          }
          CONSONANT
        }
        in "ㅏ".."ㅣ" -> {
          if (currentCheon == '·' || currentCheon == '‥') {
            if (moDictionary(currentCheon, getStableComposeesValue(i)) != '0') {
              //currentJungsung = moDictionary(currentJungsung, getStableComposeesValue(i))
              when (state) {
                "O" -> {
                  uncommittedBuilder.append(uncommitCompleteChar(jungsung = moDictionary(currentCheon, getStableComposeesValue(i))))
                  state = "M"
                }
                "J" -> {
                  uncommittedBuilder.append(uncommitCompleteChar(currentChosung, moDictionary(currentCheon, getStableComposeesValue(i))))
                  state = state + "M"
                }
                "JMJ", "JMMJ", "JMMMJ" -> {
                  committedBuilder.append(commitCompleteChar(currentChosung, currentJungsung))
                  consumedComposeeCounter += state.length + namedElementConsumedCount
                  uncommittedBuilder.append(uncommitCompleteChar(currentJongsung, moDictionary(currentCheon, getStableComposeesValue(i))))
                  state = "JM"
                }
                "JMJJ", "JMMJJ", "JMMMJJ" -> {
                  committedBuilder.append(commitCompleteChar(currentChosung, currentJungsung, getFirstJa(currentJongsung)))
                  consumedComposeeCounter += state.length + namedElementConsumedCount
                  uncommittedBuilder.append(uncommitCompleteChar(getSecondJa(currentJongsung), moDictionary(currentCheon, getStableComposeesValue(i))))
                  state = "JM"
                }
              }
              currentCheon = '\u0000'
              continue@loop
            } else {
              when (state) {
                "J", "JJ" -> {
                  committedBuilder.append(commitCompleteChar(currentChosung))
                }
                "JMJ", "JMMJ", "JMMMJ" -> {
                  committedBuilder.append(commitCompleteChar(currentChosung, currentJungsung))
                }
                "JMJJ", "JMMJJ", "JMMMJJ" -> {
                  committedBuilder.append(commitCompleteChar(currentChosung, currentJongsung))
                }
              }
              committedBuilder.append(currentCheon)
              consumedComposeeCounter += state.length + namedElementConsumedCount + 1
              state = "O"
              currentCheon = '\u0000'
            }
          }
          VOWEL
        }

        "ko_kr_h_ceon" -> {
          if (currentCheon == '·' || currentCheon == '‥') {
            when (state) {
              "J", "JJ" -> {
                committedBuilder.append(commitCompleteChar(currentChosung))
                consumedComposeeCounter += state.length + namedElementConsumedCount
              }
              "M", "MM", "MMM" -> {
                committedBuilder.append(commitCompleteChar(jungsung = currentJungsung))
                consumedComposeeCounter += state.length + namedElementConsumedCount
              }
              "JM", "JMM", "JMMM" -> {
                committedBuilder.append(commitCompleteChar(currentChosung, currentJungsung))
                consumedComposeeCounter += state.length + namedElementConsumedCount
              }
              "JMJ", "JMJJ", "JMMJ", "JMMMJ", "JMMJJ", "JMMMJJ" -> {
                committedBuilder.append(commitCompleteChar(currentChosung, currentJungsung, currentJongsung))
                consumedComposeeCounter += state.length + namedElementConsumedCount
              }
            }
            committedBuilder.append(currentCheon)
            consumedComposeeCounter += 1
            uncommittedBuilder.append('·')
            currentCheon = '·'
            state = "O"
            continue@loop
          }

          when (state) {
            "M", "MM", "MMM" -> {
              if (moDictionary(currentJungsung, '·') != '0')
                uncommittedBuilder.append(uncommitCompleteChar(jungsung = moDictionary(currentJungsung, '·')))
              else {
                committedBuilder.append(commitCompleteChar(jungsung = currentJungsung))
                consumedComposeeCounter += state.length + namedElementConsumedCount
                currentCheon = '·'
                uncommittedBuilder.append('·')
              }
            }
            "JM", "JMM", "JMMM" -> {
              if (moDictionary(currentJungsung, '·') != '0')
                uncommittedBuilder.append(uncommitCompleteChar(chosung = currentChosung, jungsung = moDictionary(currentJungsung, '·')))
              else {
                committedBuilder.append(commitCompleteChar(chosung = currentChosung, jungsung = currentJungsung))
                consumedComposeeCounter += state.length + 1 + namedElementConsumedCount
                currentCheon = '·'
                uncommittedBuilder.append('·')
              }
            }
            else -> {
              when (state) {
                "J", "JJ" -> {
                  uncommittedBuilder.append(uncommitCompleteChar(currentChosung))
                }
                "JMJ", "JMMJ", "JMMMJ" -> {
                  uncommittedBuilder.append(uncommitCompleteChar(currentChosung, currentJungsung, currentJongsung))
                }
                "JMJJ", "JMMJJ", "JMMMJJ" -> {
                  uncommittedBuilder.append(uncommitCompleteChar(currentChosung, currentJungsung, currentJongsung))
                }
              }
              currentCheon = '·'
              uncommittedBuilder.append('·')
            }
          }
          namedElementConsumedCount++
          //NAMED_ELEMENT
          continue@loop
        }
        "ko_kr_h_ceonceon" -> {
          if (currentCheon == '·' || currentCheon == '‥') {
            when (state) {
              "J", "JJ" -> {
                committedBuilder.append(commitCompleteChar(currentChosung))
                consumedComposeeCounter += state.length + namedElementConsumedCount
              }
              "M", "MM", "MMM" -> {
                committedBuilder.append(commitCompleteChar(jungsung = currentJungsung))
                consumedComposeeCounter += state.length + namedElementConsumedCount
              }
              "JM", "JMM", "JMMM" -> {
                committedBuilder.append(commitCompleteChar(currentChosung, currentJungsung))
                consumedComposeeCounter += state.length + namedElementConsumedCount
              }
              "JMJ", "JMJJ", "JMMJ", "JMMMJ", "JMMJJ", "JMMMJJ" -> {
                committedBuilder.append(commitCompleteChar(currentChosung, currentJungsung, currentJongsung))
                consumedComposeeCounter += state.length + namedElementConsumedCount
              }
            }
            committedBuilder.append(currentCheon)
            consumedComposeeCounter += 1
            uncommittedBuilder.append('‥')
            currentCheon = '‥'
            state = "O"
            continue@loop
          }

          when (state) {
            "M", "MM", "MMM" -> {
              if (moDictionary(currentJungsung, '‥') != '0')
                uncommittedBuilder.append(uncommitCompleteChar(jungsung = moDictionary(currentJungsung, '‥')))
              else {
                committedBuilder.append(commitCompleteChar(jungsung = currentJungsung))
                consumedComposeeCounter += state.length + namedElementConsumedCount
                currentCheon = '‥'
                uncommittedBuilder.append('‥')
              }
            }
            "JM", "JMM", "JMMM" -> {
              if (moDictionary(currentJungsung, '‥') != '0')
                uncommittedBuilder.append(uncommitCompleteChar(chosung = currentChosung, jungsung = moDictionary(currentJungsung, '‥')))
              else {
                committedBuilder.append(commitCompleteChar(chosung = currentChosung, jungsung = currentJungsung))
                consumedComposeeCounter += state.length + namedElementConsumedCount
                currentCheon = '‥'
                uncommittedBuilder.append('‥')
              }
            }
            else -> {
              when (state) {
                "J", "JJ" -> {
                  uncommittedBuilder.append(uncommitCompleteChar(currentChosung))
                }
                "JMJ", "JMMJ", "JMMMJ" -> {
                  uncommittedBuilder.append(uncommitCompleteChar(currentChosung, currentJungsung, currentJongsung))
                }
                "JMJJ", "JMMJJ", "JMMMJJ" -> {
                  uncommittedBuilder.append(uncommitCompleteChar(currentChosung, currentJungsung, currentJongsung))
                }
              }
              currentCheon = '‥'
              uncommittedBuilder.append('‥')
            }
          }
          namedElementConsumedCount++
          //NAMED_ELEMENT
          continue@loop
        }
        "ko_kr_h_hoeg" -> {
          when (state) {
            "J", "JJ" -> {
              if (getOverlapConsonantorVowel(currentChosung) != '0')
                currentChosung = getOverlapConsonantorVowel(currentChosung)
              uncommittedBuilder.append(uncommitCompleteChar(currentChosung))
            }
            "M", "MM", "MMM", "JM", "JMM", "JMMM" -> {
              if (getOverlapConsonantorVowel(currentJungsung) != '0')
                currentJungsung = getOverlapConsonantorVowel(currentJungsung)
              uncommittedBuilder.append(uncommitCompleteChar(currentChosung, currentJungsung))
            }

            "JMJ", "JMMJ", "JMMMJ" -> {
              if (getOverlapConsonantorVowel(currentJongsung) != '0')
                currentJongsung = getOverlapConsonantorVowel(currentJongsung)
              uncommittedBuilder.append(uncommitCompleteChar(currentChosung, currentJungsung, currentJongsung))
            }
            "JMJJ", "JMMJJ", "JMMMJJ" -> {
              if (getOverlapConsonantorVowel(getSecondJa(currentJongsung)) != '0') {
                var tmpCurrentJongsung = currentJongsung
                committedBuilder.append(commitCompleteChar(currentChosung, currentJungsung, getFirstJa(currentJongsung)))
                consumedComposeeCounter += state.length - 1 + namedElementConsumedCount
                currentChosung = getOverlapConsonantorVowel(getSecondJa(tmpCurrentJongsung))
                uncommittedBuilder.append(uncommitCompleteChar(currentChosung))
                state = "J"
              }
            }
          }
          namedElementConsumedCount++
          continue@loop
        }
        "ko_kr_h_ssang" -> {
          when (state) {
            "J", "JJ" -> {
              if (getDoubleConsonant(currentChosung) != '0')
                currentChosung = getDoubleConsonant(currentChosung)
              uncommittedBuilder.append(uncommitCompleteChar(currentChosung))
            }

            "JMJ", "JMMJ", "JMMMJ" -> {
              if (getDoubleConsonant(currentJongsung) != '0') {
                if (getJongsungIndex(getDoubleConsonant(currentJongsung)) != NOT_ADEQUATE_GRAPHEME) {
                  uncommittedBuilder.append(uncommitCompleteChar(currentChosung, currentJungsung, getDoubleConsonant(currentJongsung)))
                } else {
                  var tmpCurrentJongsung = currentJongsung
                  committedBuilder.append(commitCompleteChar(currentChosung, currentJungsung))
                  consumedComposeeCounter += state.length - 1 + namedElementConsumedCount
                  currentChosung = getDoubleConsonant(tmpCurrentJongsung)
                  uncommittedBuilder.append(uncommitCompleteChar(currentChosung))
                  state = "J"
                }
              } else {
                uncommittedBuilder.append(uncommitCompleteChar(currentChosung, currentJungsung, currentJongsung))
              }
            }
            "JMJJ", "JMMJJ", "JMMMJJ" -> {
              if (getDoubleConsonant(getSecondJa(currentJongsung)) != '0') {
                var tmpCurrentJongsung = currentJongsung
                committedBuilder.append(commitCompleteChar(currentChosung, currentJungsung, getFirstJa(currentJongsung)))
                consumedComposeeCounter += state.length - 1 + namedElementConsumedCount
                currentChosung = getDoubleConsonant(getSecondJa(tmpCurrentJongsung))
                uncommittedBuilder.append(uncommitCompleteChar(currentChosung))
                state = "J"
              } else {
                uncommittedBuilder.append(uncommitCompleteChar(currentChosung, currentJungsung, currentJongsung))
              }
            }
            "M", "MM", "MMM" -> {
              uncommittedBuilder.append(uncommitCompleteChar(jungsung = currentJungsung))
            }
            "JM", "JMM", "JMMM" -> {
              uncommittedBuilder.append(uncommitCompleteChar(chosung = currentChosung, jungsung = currentJungsung))
            }
          }
          namedElementConsumedCount++
          continue@loop
        }
        else -> {
          committedBuilder.append(uncommittedBuilder)
          consumedComposeeCounter += state.length + namedElementConsumedCount
          committedBuilder.append(getStableComposeesValue(i))
          state = "O"
          continue@loop
        }
      }

      state = when (state) {
        "O" -> {
          when (consonantOrVowel) {
            CONSONANT -> {
              uncommittedBuilder.append(uncommitCompleteChar(chosung = getStableComposeesValue(i)))
              "J"
            }
            else -> {
              uncommittedBuilder.append(uncommitCompleteChar(jungsung = getStableComposeesValue(i)))
              "M"
            }
          }
        }
        "J" -> {
          when (consonantOrVowel) {
            CONSONANT -> {
              if (jaDictionary(currentChosung, getStableComposeesValue(i)) != '0') {
                uncommittedBuilder.append(
                  uncommitCompleteChar(
                    chosung = jaDictionary(currentChosung, getStableComposeesValue(i))
                  )
                )
                "JJ"
              } else {
                committedBuilder.append(commitCompleteChar(chosung = currentChosung))
                consumedComposeeCounter += 1 + namedElementConsumedCount
                uncommittedBuilder.append(uncommitCompleteChar(chosung = getStableComposeesValue(i)))
                "J"
              }
            }
            else -> {
              uncommittedBuilder.append(uncommitCompleteChar(chosung = currentChosung, jungsung = getStableComposeesValue(i)))
              "JM"
            }
          }
        }
        "M" -> {
          when (consonantOrVowel) {
            CONSONANT -> {
              committedBuilder.append(commitCompleteChar(jungsung = currentJungsung))
              consumedComposeeCounter += 1 + namedElementConsumedCount
              uncommittedBuilder.append(uncommitCompleteChar(chosung = getStableComposeesValue(i)))
              "J"
            }
            else -> {
              if (moDictionary(currentJungsung, getStableComposeesValue(i)) != '0') {
                uncommittedBuilder.append(
                  uncommitCompleteChar(
                    jungsung = moDictionary(currentJungsung, getStableComposeesValue(i))
                  )
                )
                "MM"
              } else {
                committedBuilder.append(commitCompleteChar(jungsung = currentJungsung))
                consumedComposeeCounter += 1 + namedElementConsumedCount
                uncommittedBuilder.append(uncommitCompleteChar(jungsung = getStableComposeesValue(i)))
                "M"
              }
            }
          }
        }
        "JJ" -> {
          when (consonantOrVowel) {
            CONSONANT -> {
              committedBuilder.append(commitCompleteChar(chosung = currentChosung))
              consumedComposeeCounter += 2 + namedElementConsumedCount
              uncommittedBuilder.append(uncommitCompleteChar(chosung = getStableComposeesValue(i)))
              "J"
            }
            else -> {
              committedBuilder.append(commitCompleteChar(chosung = getFirstJa(currentChosung)))
              consumedComposeeCounter += 1 + namedElementConsumedCount
              uncommittedBuilder.append(uncommitCompleteChar(chosung = getSecondJa(currentChosung), jungsung = getStableComposeesValue(i)))
              "JM"
            }

          }
        }
        "MM" -> {
          when (consonantOrVowel) {
            CONSONANT -> {
              committedBuilder.append(commitCompleteChar(jungsung = currentJungsung))
              consumedComposeeCounter += 2 + namedElementConsumedCount
              uncommittedBuilder.append(uncommitCompleteChar(chosung = getStableComposeesValue(i)))
              "J"
            }
            else -> {
              if (moDictionary(currentJungsung, getStableComposeesValue(i)) != '0') {
                uncommittedBuilder.append(
                  uncommitCompleteChar(
                    jungsung = moDictionary(
                      currentJungsung,
                      getStableComposeesValue(i)
                    )
                  )
                )
                "MMM"
              } else {
                committedBuilder.append(commitCompleteChar(jungsung = currentJungsung))
                consumedComposeeCounter += 2 + namedElementConsumedCount
                uncommittedBuilder.append(uncommitCompleteChar(jungsung = getStableComposeesValue(i)))
                "M"
              }
            }
          }
        }
        "MMM" -> {
          when (consonantOrVowel) {
            CONSONANT -> {
              committedBuilder.append(
                commitCompleteChar(jungsung = currentJungsung)
              )
              consumedComposeeCounter += 3 + namedElementConsumedCount
              uncommittedBuilder.append(uncommitCompleteChar(chosung = getStableComposeesValue(i)))
              "J"
            }
            else -> {
              committedBuilder.append(
                commitCompleteChar(jungsung = currentJungsung)
              )
              consumedComposeeCounter += 3 + namedElementConsumedCount
              uncommittedBuilder.append(uncommitCompleteChar(jungsung = getStableComposeesValue(i)))
              "M"
            }
          }
        }
        "JM" -> {
          when (consonantOrVowel) {
            CONSONANT -> {
              if (getJongsungIndex(getStableComposeesValue(i)) != NOT_ADEQUATE_GRAPHEME) {
                uncommittedBuilder.append(
                  uncommitCompleteChar(
                    chosung = currentChosung,
                    jungsung = currentJungsung,
                    jongsung = getStableComposeesValue(i)
                  )
                )
                "JMJ"
              } else {
                committedBuilder.append(commitCompleteChar(chosung = currentChosung, jungsung = currentJungsung))
                consumedComposeeCounter += 2 + namedElementConsumedCount
                uncommittedBuilder.append(
                  uncommitCompleteChar(
                    chosung = getStableComposeesValue(i)
                  )
                )
                "J"
              }
            }
            else -> {
              if (moDictionary(currentJungsung, getStableComposeesValue(i)) != '0') {
                uncommittedBuilder.append(
                  uncommitCompleteChar(
                    chosung = currentChosung,
                    jungsung = moDictionary(currentJungsung, getStableComposeesValue(i))
                  )
                )
                "JMM"
              } else {
                committedBuilder.append(commitCompleteChar(chosung = currentChosung, jungsung = currentJungsung))
                consumedComposeeCounter += 2 + namedElementConsumedCount
                uncommittedBuilder.append(uncommitCompleteChar(jungsung = getStableComposeesValue(i)))
                "M"
              }
            }
          }
        }
        "JMJ" -> {
          when (consonantOrVowel) {
            CONSONANT -> {
              if (jaDictionary(currentJongsung, getStableComposeesValue(i)) != '0') {
                uncommittedBuilder.append(
                  uncommitCompleteChar(
                    chosung = currentChosung,
                    jungsung = currentJungsung,
                    jongsung = jaDictionary(currentJongsung, getStableComposeesValue(i))
                  )
                )
                "JMJJ"
              } else {
                committedBuilder.append(commitCompleteChar(currentChosung, currentJungsung, currentJongsung))
                consumedComposeeCounter += 3 + namedElementConsumedCount
                uncommittedBuilder.append(uncommitCompleteChar(chosung = getStableComposeesValue(i)))
                "J"
              }
            }
            else -> {
              committedBuilder.append(commitCompleteChar(currentChosung, currentJungsung))
              consumedComposeeCounter += 2 + namedElementConsumedCount
              uncommittedBuilder.append(uncommitCompleteChar(chosung = currentJongsung, jungsung = getStableComposeesValue(i)))
              "JM"
            }
          }

        }
        "JMM" -> {
          when (consonantOrVowel) {
            CONSONANT -> {
              if (getJongsungIndex(getStableComposeesValue(i)) != NOT_ADEQUATE_GRAPHEME) {
                uncommittedBuilder.append(
                  uncommitCompleteChar(
                    chosung = currentChosung,
                    jungsung = currentJungsung,
                    jongsung = getStableComposeesValue(i)
                  )
                )
                "JMMJ"
              } else {
                committedBuilder.append(
                  commitCompleteChar(
                    chosung = currentChosung,
                    jungsung = currentJungsung
                  )
                )
                consumedComposeeCounter += 3 + namedElementConsumedCount
                uncommittedBuilder.append(uncommitCompleteChar(chosung = getStableComposeesValue(i)))
                "J"
              }
            }
            else -> {
              if (moDictionary(currentJungsung, getStableComposeesValue(i)) != '0') {
                uncommittedBuilder.append(
                  uncommitCompleteChar(
                    chosung = currentChosung,
                    jungsung = moDictionary(currentJungsung, getStableComposeesValue(i))
                  )
                )
                "JMMM"
              } else {
                committedBuilder.append(
                  commitCompleteChar(
                    chosung = currentChosung,
                    jungsung = currentJungsung
                  )
                )
                consumedComposeeCounter += 2 + namedElementConsumedCount
                uncommittedBuilder.append(uncommitCompleteChar(jungsung = getStableComposeesValue(i)))
                "M"
              }
            }
          }

        }
        "JMMM" -> {
          when (consonantOrVowel) {
            CONSONANT -> {
              if (getJongsungIndex(getStableComposeesValue(i)) != NOT_ADEQUATE_GRAPHEME) {
                uncommittedBuilder.append(
                  uncommitCompleteChar(
                    chosung = currentChosung,
                    jungsung = currentJungsung,
                    jongsung = getStableComposeesValue(i)
                  )
                )
                "JMMMJ"
              } else {
                committedBuilder.append(
                  commitCompleteChar(
                    chosung = currentChosung,
                    jungsung = currentJungsung
                  )
                )
                consumedComposeeCounter += 4 + namedElementConsumedCount
                uncommittedBuilder.append(
                  uncommitCompleteChar(
                    chosung = getStableComposeesValue(i)
                  )
                )
                "J"
              }
            }

            else -> {
              committedBuilder.append(
                commitCompleteChar(
                  chosung = currentChosung,
                  jungsung = currentJungsung
                )
              )
              consumedComposeeCounter += 4 + namedElementConsumedCount
              uncommittedBuilder.append(uncommitCompleteChar(jungsung = getStableComposeesValue(i)))
              "M"
            }

          }
        }
        "JMJJ" -> {
          when (consonantOrVowel) {
            CONSONANT -> {
              committedBuilder.append(
                commitCompleteChar(
                  currentChosung,
                  currentJungsung,
                  currentJongsung
                )
              )
              consumedComposeeCounter += 4 + namedElementConsumedCount
              uncommittedBuilder.append(uncommitCompleteChar(chosung = getStableComposeesValue(i)))
              "J"
            }
            else -> {
              committedBuilder.append(commitCompleteChar(currentChosung, currentJungsung, getFirstJa(currentJongsung)))
              consumedComposeeCounter += 3 + namedElementConsumedCount
              uncommittedBuilder.append(uncommitCompleteChar(chosung = getSecondJa(currentJongsung), jungsung = getStableComposeesValue(i)))
              "JM"
            }
          }

        }
        "JMMJ" -> {
          when (consonantOrVowel) {
            CONSONANT -> {
              if (jaDictionary(currentJongsung, getStableComposeesValue(i)) != '0') {
                uncommittedBuilder.append(
                  uncommitCompleteChar(
                    chosung = currentChosung,
                    jungsung = currentJungsung,
                    jongsung = jaDictionary(currentJongsung, getStableComposeesValue(i))
                  )
                )
                "JMMJJ"
              } else {
                committedBuilder.append(
                  commitCompleteChar(
                    currentChosung,
                    currentJungsung,
                    currentJongsung
                  )
                )
                consumedComposeeCounter += 4 + namedElementConsumedCount
                uncommittedBuilder.append(uncommitCompleteChar(chosung = getStableComposeesValue(i)))
                "J"
              }
            }
            else -> {
              committedBuilder.append(
                commitCompleteChar(
                  currentChosung,
                  currentJungsung
                )
              )
              consumedComposeeCounter += 3 + namedElementConsumedCount
              uncommittedBuilder.append(uncommitCompleteChar(chosung = currentJongsung, jungsung = getStableComposeesValue(i)))
              "JM"
            }
          }
        }
        "JMMMJ" -> {
          when (consonantOrVowel) {
            CONSONANT -> {
              if (jaDictionary(currentJongsung, getStableComposeesValue(i)) != '0') {
                uncommittedBuilder.append(
                  uncommitCompleteChar(
                    chosung = currentChosung,
                    jungsung = currentJungsung,
                    jongsung = jaDictionary(currentJongsung, getStableComposeesValue(i))
                  )
                )
                "JMMMJJ"
              } else {
                committedBuilder.append(
                  commitCompleteChar(
                    chosung = currentChosung,
                    jungsung = currentJungsung,
                    jongsung = currentJongsung
                  )
                )
                consumedComposeeCounter += 5 + namedElementConsumedCount
                uncommittedBuilder.append(
                  uncommitCompleteChar(chosung = getStableComposeesValue(i))
                )
                "J"
              }
            }
            else -> {
              committedBuilder.append(
                commitCompleteChar(
                  chosung = currentChosung,
                  jungsung = currentJungsung
                )
              )
              consumedComposeeCounter += 4 + namedElementConsumedCount
              uncommittedBuilder.append(uncommitCompleteChar(chosung = getStableComposeesValue(i - 1), jungsung = getStableComposeesValue(i)))
              "JM"
            }
          }
        }
        "JMMJJ" -> {
          when (consonantOrVowel) {
            CONSONANT -> {
              committedBuilder.append(
                commitCompleteChar(
                  currentChosung,
                  currentJungsung,
                  currentJongsung
                )
              )
              consumedComposeeCounter += 5 + namedElementConsumedCount
              uncommittedBuilder.append(uncommitCompleteChar(chosung = getStableComposeesValue(i)))
              "J"
            }
            else -> {
              committedBuilder.append(
                commitCompleteChar(
                  currentChosung,
                  currentJungsung,
                  getFirstJa(currentJongsung)
                )
              )
              consumedComposeeCounter += 4 + namedElementConsumedCount
              uncommittedBuilder.append(uncommitCompleteChar(chosung = getSecondJa(currentJongsung), jungsung = getStableComposeesValue(i)))
              "JM"
            }
          }
        }
        "JMMMJJ" -> {
          when (consonantOrVowel) {
            CONSONANT -> {
              committedBuilder.append(
                commitCompleteChar(
                  chosung = currentChosung,
                  jungsung = currentJungsung,
                  jongsung = currentJongsung
                )
              )
              consumedComposeeCounter += 6 + namedElementConsumedCount
              uncommittedBuilder.append(uncommitCompleteChar(chosung = getStableComposeesValue(i)))
              "J"
            }
            else -> {
              committedBuilder.append(
                commitCompleteChar(
                  chosung = currentChosung,
                  jungsung = currentJungsung,
                  jongsung = getFirstJa(currentJongsung)
                )
              )
              consumedComposeeCounter += 5 + namedElementConsumedCount
              uncommittedBuilder.append(uncommitCompleteChar(chosung = getSecondJa(currentJongsung), jungsung = getStableComposeesValue(i)))
              "JM"
            }
          }
        }
        else -> {
          throw NoWhenBranchMatchedException("Wrong state")
        }
      }
    }
  }
}