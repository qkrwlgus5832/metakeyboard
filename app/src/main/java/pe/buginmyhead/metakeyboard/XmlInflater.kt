package pe.buginmyhead.metakeyboard

import java.io.InputStream
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import org.xmlpull.v1.XmlPullParserFactory

object XmlInflater {
  private val parser = XmlPullParserFactory.newInstance().newPullParser()

  fun rowBasedKeyboard(inputStream: InputStream): RowBasedKeyboard {
    parser.setInput(inputStream, "utf-8")

    var ret: RowBasedKeyboard = rowBasedKeyboard {}

    while (parser.eventType != XmlPullParser.END_DOCUMENT) {
      when (parser.eventType) {
        XmlPullParser.START_TAG -> {
          when (parser.name) {
            "row-based-keyboard-v0-experimental" -> {
              ret = rowBasedKeyboard {
                parser.getAttributeValue(null, "orientation")?.run {
                  orientation = RowBasedKeyboard.Orientation.valueOf(this.toUpperCase())
                }

                while (true) {
                  parser.nextTag()
                  if (
                      parser.eventType == XmlPullParser.END_TAG
                      && parser.name == "row-based-keyboard-v0-experimental"
                  ) {
                    break
                  } else if (parser.eventType == XmlPullParser.START_TAG) when (parser.name) {
                    "keyboard-row" -> keyboardRow()
                  }
                }
              }
            }
          }
        }
      }
      try {
        parser.nextTag()
      } catch (Exception: XmlPullParserException) {
        return ret
      }
    }
    return ret
  }

  private fun RowBasedKeyboard.Builder.keyboardRow() {
    keyboardRow {
      parser.getAttributeValue(null, "weight")?.run {
        weight = this.toFloat()
      }

      while (true) {
        parser.nextTag()
        if (
            parser.eventType == XmlPullParser.END_TAG
            && parser.name == "keyboard-row"
        ) {
          break
        } else if (parser.eventType == XmlPullParser.START_TAG) when (parser.name) {
          "base-button" -> baseButton()
          "repeater-button" -> repeaterButton()
          "custom-long-click-button" -> customLongClickButton()
          "popup-button" -> TODO("not implemented")
        }
      }
    }
  }

  private fun RowBasedKeyboard.KeyboardRow.Builder.baseButton() {
    baseButton {
      parser.getAttributeValue(null, "style")?.run {
        style = RowBasedKeyboard.AbstractButton.Style.valueOf(this.toUpperCase())
      }
      parser.getAttributeValue(null, "weight")?.run {
        weight = this.toFloat()
      }

      while (true) {
        parser.nextTag()
        if (
            parser.eventType == XmlPullParser.END_TAG
            && parser.name == "base-button"
        ) {
          break
        } else if (parser.eventType == XmlPullParser.START_TAG) reaction {
          reaction()
        }
      }
    }
  }

  private fun RowBasedKeyboard.KeyboardRow.Builder.repeaterButton() {
    repeaterButton {
      parser.getAttributeValue(null, "style")?.run {
        style = RowBasedKeyboard.AbstractButton.Style.valueOf(this.toUpperCase())
      }
      parser.getAttributeValue(null, "weight")?.run {
        weight = this.toFloat()
      }
      parser.getAttributeValue(null, "rate")?.run {
        rate = RowBasedKeyboard.AbstractButton.RepeaterButton.Rate.valueOf(this.toUpperCase())
      }

      while (true) {
        parser.nextTag()
        if (
            parser.eventType == XmlPullParser.END_TAG
            && parser.name == "repeater-button"
        ) break
        else if (parser.eventType == XmlPullParser.START_TAG) when (parser.name) {
          "button-text" -> buttonText()
          "long-click" -> longClick()
          "short-click" -> shortClick()
        }
      }
    }
  }

  private fun RowBasedKeyboard.AbstractButton.RepeaterButton.Builder.longClick() {
    longClickReaction {
      reaction("long-click")
    }
  }

  private fun RowBasedKeyboard.AbstractButton.RepeaterButton.Builder.shortClick() {
    shortClickReaction {
      reaction("short-click")
    }
  }

  private fun RowBasedKeyboard.KeyboardRow.Builder.customLongClickButton() {
    customLongClickButton {
      parser.getAttributeValue(null, "style")?.run {
        style = RowBasedKeyboard.AbstractButton.Style.valueOf(this.toUpperCase())
      }
      parser.getAttributeValue(null, "weight")?.run {
        weight = this.toFloat()
      }
      parser.getAttributeValue(null, "time-limit")?.run {
        hasTimeLimit = this.toBoolean()
      }
      parser.getAttributeValue(null, "one-way")?.run {
        isOneWay = this.toBoolean()
      }

      while (true) {
        parser.nextTag()
        if (
            parser.eventType == XmlPullParser.END_TAG
            && parser.name == "custom-long-click-button"
        ) break
        else if (parser.eventType == XmlPullParser.START_TAG) when (parser.name) {
          "button-text" -> buttonText()
          "long-click" -> longClick()
          "short-click" -> shortClick()
        }
      }
    }
  }

  private fun RowBasedKeyboard.AbstractButton.AbstractSwitcherButton.CustomLongClickButton.Builder.longClick() {
    longClickReaction {
      reaction("long-click")
    }
  }

  private fun RowBasedKeyboard.AbstractButton.AbstractSwitcherButton.CustomLongClickButton.Builder.shortClick() {
    shortClickReaction {
      reaction("short-click")
    }
  }

  private fun arguments(): List<MetakeyboardModel.Callee.Argument> {
    val ret = mutableListOf<MetakeyboardModel.Callee.Argument>()
    while (true) {
      parser.nextTag()
      if (
          parser.eventType == XmlPullParser.END_TAG
          && parser.name == "call"
      ) {
        break
      } else {
        when (parser.name) {
          "argument" -> ret.add(MetakeyboardModel.Callee.Argument(
              MetakeyboardModel.Callee.Argument.Type.valueOf(
                  parser.getAttributeValue(null, "type").toUpperCase()
              ),
              parser.getAttributeValue(null, "value")
          ))
        }
      }
    }
    return ret
  }

  private fun RowBasedKeyboard.Reaction.Builder.reaction() {
    if (parser.eventType == XmlPullParser.START_TAG)
      when (parser.name) {
        "button-text" -> buttonText()
        "call" -> callable(
            MetakeyboardLayout.Callee(
                parser.getAttributeValue(null, "function"),
                arguments()
            )
        )
        "compose" -> callable(
            MetakeyboardLayout.Composee(
                PrimitiveComposer.Category.valueOf(parser.getAttributeValue(null, "category").toUpperCase()),
                parser.getAttributeValue(null, "value"),
                when (parser.getAttributeValue(null, "composer")) {
                  "hangul" -> Hangul::class
                  else -> null
                },
                parser.getAttributeValue(null, "print")
            )
        )
      }
  }

  private fun RowBasedKeyboard.Reaction.Builder.reaction(tagName: String) {
    while (true) {
      parser.nextTag()
      if (
          parser.eventType == XmlPullParser.END_TAG
          && parser.name == tagName
      ) {
        break
      } else {
        reaction()
      }
    }
  }

  private fun RowBasedKeyboard.AbstractButton.Builder.buttonText() {
    buttonText {
      buttonText()
    }
  }

  private fun RowBasedKeyboard.Reaction.Builder.buttonText() {
    buttonText {
      buttonText()
    }
  }

  private fun RowBasedKeyboard.ButtonText.Builder.buttonText() {
    while (true) {
      parser.nextTag()
      if (
          parser.eventType == XmlPullParser.END_TAG
          && parser.name == "button-text"
      ) break
      else if (parser.eventType == XmlPullParser.START_TAG) when (parser.name) {
        "text" -> text {
          parser.getAttributeValue(null, "text")?.run {
            text = this
          }
        }
      }
    }
  }
}