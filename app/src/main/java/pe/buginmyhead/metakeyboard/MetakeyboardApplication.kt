package pe.buginmyhead.metakeyboard

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.PixelFormat
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.Gravity
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.documentfile.provider.DocumentFile
import androidx.preference.PreferenceManager

class MetakeyboardApplication : Application(),
    SharedPreferences.OnSharedPreferenceChangeListener {
  companion object {
    var keyboard: RowBasedKeyboard = rowBasedKeyboard {}

    @SuppressLint("InlinedApi")
    fun requestOverlayPermission(context: Context): Boolean {
      return (
          Build.VERSION.SDK_INT < Build.VERSION_CODES.M
              || Settings.canDrawOverlays(context)
          ).also {
        if (!it) context.startActivity(
            Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:${context.packageName}"))
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK),
            null
        )
      }
    }

    fun onSharedPreferenceChanged(key:String, keyboardView: RowBasedKeyboardView) {
      when (key) {
        "keyboard_height", "master_row_width", "load_keyboard_layout", "keyboard_background_image", "keyboard_background_image_policy" -> {
          keyboardView.requestLayout()
          // I don't know why the following instruction is needed but if I omit it, the view doesn't reflect changes immediately.
          keyboardView.invalidate()
        }
      }
    }
  }

  override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
    Companion.onSharedPreferenceChanged(key, keyboardView)
  }

  private lateinit var keyboardView: RowBasedKeyboardView//MetakeyboardView
  private lateinit var keyboardBorderAdjusterLayout: KeyboardBorderAdjusterLayout

  override fun onCreate() {
    super.onCreate()
    // To avoid "android.content.res.Resources$NotFoundException: Resource ID #0x0"
    setTheme(R.style.AppTheme)

    PreferenceManager
        .getDefaultSharedPreferences(this)
        .registerOnSharedPreferenceChangeListener(this)

    keyboardView = RowBasedKeyboardView(this)
    keyboardBorderAdjusterLayout = KeyboardBorderAdjusterLayout(this).apply {
      removeViewFromWindow = {
        (getSystemService(Context.WINDOW_SERVICE) as WindowManager).removeView(this)
      }
      orientation = LinearLayout.VERTICAL
      gravity = Gravity.BOTTOM

      addView(KeyboardHeightControlBar(this@MetakeyboardApplication))
      addView(keyboardView)
    }

    MetakeyboardModel.showKeyboardListCallback = {
      val treeUri = Uri.parse(
          PreferenceManager
              .getDefaultSharedPreferences(this)
              .getString("load_keyboard_layout_directory", "")
      )
      try {
        val documentFile = DocumentFile.fromTreeUri(this, treeUri)!!
        val fileList = documentFile.listFiles()
        val fileNameList = fileList.map { it.name }.toTypedArray()
        val keyboardListDialog = AlertDialog.Builder(this)
            .setTitle("Choose keyboard")
            .setItems(fileNameList) { _, which ->
              PreferenceManager
                  .getDefaultSharedPreferences(this)
                  .edit()
                  .putString("load_keyboard_layout", fileList[which].uri.toString())
                  .apply()
            }
            .create()
        keyboardListDialog.window!!.setType(
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
              WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
            } else {
              WindowManager.LayoutParams.TYPE_SYSTEM_ALERT
            }
        )
        if (requestOverlayPermission(this)) {
          keyboardListDialog.show()
        }
      } catch (exc: IllegalArgumentException) {
        Toast.makeText(this, "키보드 선택을 하기 위해서는 폴더 선택이 필요합니다.", Toast.LENGTH_SHORT).show()
        // TODO: transfer extra
        startActivity(
            Intent(this, SettingsActivity::class.java)
                .putExtras(Bundle().apply { putString("setting", "select_directory") })
                .apply { flags = Intent.FLAG_ACTIVITY_NEW_TASK },
            Bundle().apply { putString("setting", "select_directory") }
        )
      }
    }
    MetakeyboardModel.openAppCallback = { args: List<String> ->
      try {
        startActivity(
            packageManager
                .apply { getApplicationInfo(args[0], PackageManager.GET_META_DATA) }
                .getLaunchIntentForPackage(args[0])
        )
      } catch (exc: PackageManager.NameNotFoundException) {
        startActivity(
            Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=${args[0]}"))
                .apply { flags = Intent.FLAG_ACTIVITY_NEW_TASK }
        )
      }
    }
    MetakeyboardModel.adjustKeyboardBorderCallback = {
      val windowType =
          if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
          else
            WindowManager.LayoutParams.TYPE_SYSTEM_ALERT
      val layoutParams = WindowManager.LayoutParams(windowType).apply {
        format = PixelFormat.TRANSLUCENT
      }
      if (MetakeyboardApplication.requestOverlayPermission(this)) {
        (getSystemService(Context.WINDOW_SERVICE) as WindowManager).addView(keyboardBorderAdjusterLayout, layoutParams)
      }
    }
    MetakeyboardModel.showSettingsCallback = {
      startActivity(
          Intent(this, SettingsActivity::class.java)
              .apply { flags = Intent.FLAG_ACTIVITY_NEW_TASK }
      )
    }
  }
}