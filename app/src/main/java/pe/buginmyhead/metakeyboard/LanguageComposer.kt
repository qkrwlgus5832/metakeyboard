package pe.buginmyhead.metakeyboard

interface LanguageComposer : PrimitiveComposer {
  fun countConsumed(): Int
}