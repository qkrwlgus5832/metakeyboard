package pe.buginmyhead.metakeyboard

interface IntegrationComposer : PrimitiveComposer {
  var lackOfCommittedTextEventHandler: () -> CharSequence

  fun push(composee: PrimitiveComposer.Composee? = null, volatility: Boolean = false)

  override fun push(composee: PrimitiveComposer.Composee) {
    push(composee, false)
  }

  fun clearVolatile()
}