package pe.buginmyhead.metakeyboard

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.view.MotionEvent
import android.view.View
import androidx.preference.PreferenceManager
import android.inputmethodservice.InputMethodService

class MetakeyboardView(
    context: Context,
    var inputMethodService: InputMethodService
) : View(context) {
  var previewEnabled = false
  var verticalCorrection = 0

  private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

  override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
    val sharedPreference = PreferenceManager.getDefaultSharedPreferences(context)
    val keyboardHeight = sharedPreference.getString("keyboard_height", "")!!.toInt()
    setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), keyboardHeight)
  }

  override fun onDraw(canvas: Canvas) {
    canvas.drawARGB(0xff, 0x7f, 0x7f, 0x00)
    paint.color = Color.RED
    paint.style = Paint.Style.FILL
    canvas.drawRect(100F, 100F, canvas.maximumBitmapWidth.toFloat(), 200F, paint)
    canvas.drawRect(100F, -100F, canvas.maximumBitmapWidth.toFloat(), 20F, paint)
  }

  @SuppressLint("ClickableViewAccessibility")
  override fun onTouchEvent(event: MotionEvent): Boolean {
    val ic = inputMethodService.currentInputConnection
    when (event.action) {
      MotionEvent.ACTION_DOWN -> {
        ic.commitText("60", 1)
      }
      MotionEvent.ACTION_MOVE -> {
      }
      MotionEvent.ACTION_UP -> {
        ic.commitText("0", 1)
      }
    }
    return true
  }
}