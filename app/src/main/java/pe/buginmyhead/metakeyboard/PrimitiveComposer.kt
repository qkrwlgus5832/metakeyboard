package pe.buginmyhead.metakeyboard

import kotlin.reflect.KClass

interface PrimitiveComposer {
  fun push(composee: Composee)

  fun pullCommitted(): CharSequence
  fun pullUncommitted(): CharSequence

  fun clearCommitted()
  fun clearUncommitted()

  enum class Category { NAMED_ELEMENT, GRAPHEME, TEXT }

  data class Composee(
    val category: Category,
    val value: String,
    val languageComposer: KClass<out LanguageComposer>?,
    val print: String? = null
  )
}