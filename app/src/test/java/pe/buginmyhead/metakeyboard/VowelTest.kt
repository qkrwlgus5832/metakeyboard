package pe.buginmyhead.metakeyboard

import io.kotlintest.shouldBe

internal class VowelTest : LanguageComposerTest<Hangul>(Hangul()) {
  init {
    "'Initial state'" {
      composer.pullCommitted().toString() shouldBe ""
      composer.pullUncommitted().toString() shouldBe ""
    }

    "ceonjiin" - {
      "vowel: ㅣ ㅏ ㅐ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅣ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅏ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅐ"
        }
      }

      "vowel: ㅑ ㅒ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅑ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅒ"
        }
      }

      "vowel: · ㅓ ㅔ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "·"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅓ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅔ"
        }
      }

      "vowel: ‥ ㅕ ㅖ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceonceon", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅖ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅗ ㅚ ㅘ ㅙ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅗ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅚ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅘ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅙ"
        }
      }

      "vowel: ㅛ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceonceon", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅛ"
        }
      }

      "vowel: ㅡ ㅜ ㅟ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅡ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅜ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅟ"
        }
      }

      "vowel: ㅠ ㅝ ㅞ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceonceon", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅠ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅝ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅞ"
        }
      }

      "vowel: ㅢ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅢ"
        }
      }
    }

    "narasgeul" - {
      "vowel: ㅏ ㅐ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅏ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅐ"
        }
      }

      "vowel: ㅑ ㅒ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_hoeg", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅑ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅒ"
        }
      }

      "vowel: ㅡ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅡ"
        }
      }

      "vowel: ㅣ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅣ"
        }
      }

      "vowel: ㅢ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅢ"
        }
      }

      "vowel: ㅗ ㅛ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅗ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_hoeg", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅛ"
        }
      }
      "vowel: ㅜ ㅠ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅜ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅜ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_hoeg", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅠ"
        }
      }

      "vowel: ㅘ ㅙ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅘ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅙ"
        }
      }

      "vowel: ㅝ ㅞ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅜ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅓ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅝ"
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅞ"
        }
      }

      "vowel: ㅚ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅚ"
        }
      }

      "vowel: ㅟ" {
        composer.run {
          clearCommitted()
          clearUncommitted()
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅜ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅟ"
        }
      }
    }

    "vega" - {
      "vowel: ㅑ ㅒ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅑ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅑ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅒ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅕ ㅖ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅕ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅕ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅖ"
          countConsumed() shouldBe 0
        }
      }
    }
  }
}