package pe.buginmyhead.metakeyboard

import io.kotlintest.TestCase
import io.kotlintest.TestResult
import io.kotlintest.extensions.TestListener
import io.kotlintest.shouldBe

internal class ConsonantTest : LanguageComposerTest<Hangul>(Hangul()) {
  init {
    "'Initial state'" {
      composer.pullCommitted().toString() shouldBe ""
      composer.pullUncommitted().toString() shouldBe ""
    }

    val consonantArray = arrayOf(
      "ㄱ", "ㄴ", "ㄷ", "ㄹ", "ㅁ",
      "ㅂ", "ㅅ", "ㅇ", "ㅈ", "ㅊ",
      "ㅋ", "ㅌ", "ㅍ", "ㅎ", "ㄲ",
      "ㄸ", "ㅃ", "ㅆ", "ㅉ"
    )
    val narasgeulConsonantHwikArray = arrayOf(
      "ㄱ", "ko_kr_h_hoeg", "ㄴ", "ko_kr_h_hoeg", "ㄴ", "ko_kr_h_hoeg", "ko_kr_h_hoeg",
      "ㅁ", "ko_kr_h_hoeg", "ㅁ", "ko_kr_h_hoeg", "ko_kr_h_hoeg", "ㅅ", "ko_kr_h_hoeg",
      "ㅅ", "ko_kr_h_hoeg", "ko_kr_h_hoeg", "ㅇ", "ko_kr_h_hoeg", " "
    )
    val narasgeulConsonantSSangArray = arrayOf(
      "ㄱ", "ko_kr_h_ssang", "ㄷ", "ko_kr_h_ssang", "ㅂ", "ko_kr_h_ssang",
      "ㅅ", "ko_kr_h_ssang", "ㅈ", "ko_kr_h_ssang", " "
    )

    composer.run {
      clearCommitted()
      clearUncommitted()
      for (c in consonantArray) {
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, c, Hangul::class, null))
      }
      push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, " ", Hangul::class, null))
    }

    "ㄱ ㄴ ㄷ ㄹ ㅁ ㅂ ㅅ ㅇ ㅈ ㅊ ㅋ ㅌ ㅍ ㅎ ㄲ ㄸ ㅃ ㅆ ㅉ" {
      composer.run {
        pullCommitted().toString() shouldBe "ㄱㄴㄷㄹㅁㅂㅅㅇㅈㅊㅋㅌㅍㅎㄲㄸㅃㅆㅉ"
        pullUncommitted().toString() shouldBe ""
      }
    }

    composer.run {
      clearCommitted()
      clearUncommitted()
      for (c in narasgeulConsonantHwikArray) {
        if (c == "ko_kr_h_hoeg" || c == " ")
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, c, Hangul::class, null))
        else
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, c, Hangul::class, null))
      }
    }

    // narasgeul
    // hoeg
    "ㅋ ㄷ ㅌ ㅂ ㅍ ㅈ ㅊ ㅎ" {
      composer.run {
        pullCommitted().toString() shouldBe "ㅋㄷㅌㅂㅍㅈㅊㅎ"
        pullUncommitted().toString() shouldBe ""
      }
    }

    composer.run {
      clearCommitted()
      clearUncommitted()
      for (c in narasgeulConsonantSSangArray) {
        if (c == "ko_kr_h_ssang" || c == " ")
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, c, Hangul::class, null))
        else
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, c, Hangul::class, null))
      }
    }
    // ssang
    "ㄲ ㄸ ㅃ ㅆ ㅉ" {
      composer.run {
        pullCommitted().toString() shouldBe "ㄲㄸㅃㅆㅉ"
        pullUncommitted().toString() shouldBe ""
      }
    }
  }
}