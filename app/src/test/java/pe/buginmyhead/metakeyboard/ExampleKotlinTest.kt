package pe.buginmyhead.metakeyboard

import io.kotlintest.shouldBe

internal class ExampleKotlinTest : LanguageComposerTest<Hangul>(Hangul()) {
  init {
    "ㄱㄱㄱ" {
      Hangul().run {
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "ㄱ"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
        pullCommitted().toString() shouldBe "ㄱ"
        pullUncommitted().toString() shouldBe "ㄱ"
        countConsumed() shouldBe 1
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
        pullCommitted().toString() shouldBe "ㄱㄱ"
        pullUncommitted().toString() shouldBe "ㄱ"
        countConsumed() shouldBe 2
      }
    }

    "간" {
      val hangul = Hangul()
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class))
      hangul.pullCommitted().toString() shouldBe ""
      hangul.pullUncommitted().toString() shouldBe "간"
    }

    "가나" {
      val hangul = Hangul()
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
      hangul.pullCommitted().toString() shouldBe "가"
      hangul.pullUncommitted().toString() shouldBe "나"
    }

    "홚" {
      val hangul = Hangul()
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class))
      hangul.pullCommitted().toString() shouldBe ""
      hangul.pullUncommitted().toString() shouldBe "홚"
    }

    "환호" {
      val hangul = Hangul()
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
      hangul.pullCommitted().toString() shouldBe "환"
      hangul.pullUncommitted().toString() shouldBe "호"
    }

    "넉살" {
      val hangul = Hangul()
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅓ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅅ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class))
      hangul.pullCommitted().toString() shouldBe "넉"
      hangul.pullUncommitted().toString() shouldBe "살"
    }

    "구글" {
      val hangul = Hangul()
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅜ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class))
      hangul.pullCommitted().toString() shouldBe "구"
      hangul.pullUncommitted().toString() shouldBe "글"
    }

    "카톡" {
      val hangul = Hangul()
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅋ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅌ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
      hangul.pullCommitted().toString() shouldBe "카"
      hangul.pullUncommitted().toString() shouldBe "톡"
    }

    "깒" {
      val hangul = Hangul()
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄲ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class))
      hangul.pullCommitted().toString() shouldBe ""
      hangul.pullUncommitted().toString() shouldBe "깒"
    }

    "눼" {
      val hangul = Hangul()
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅜ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅓ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
      hangul.pullCommitted().toString() shouldBe ""
      hangul.pullUncommitted().toString() shouldBe "눼"
    }

    "왜그래" {
      val hangul = Hangul()
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅐ", Hangul::class))
      hangul.pullCommitted().toString() shouldBe "왜그"
      hangul.pullUncommitted().toString() shouldBe "래"
    }

    "왰 - 3중 모음" {
      val hangul = Hangul()
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅆ", Hangul::class))

      hangul.pullCommitted().toString() shouldBe ""
      hangul.pullUncommitted().toString() shouldBe "왰"
    }

    "궤양 - 2중 모음" {
      val hangul = Hangul()
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅜ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅔ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅑ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))

      hangul.pullCommitted().toString() shouldBe "궤"
      hangul.countConsumed() shouldBe 3
      hangul.pullUncommitted().toString() shouldBe "양"
    }

    "왰 - 2중 모음" {
      val hangul = Hangul()
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅐ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅆ", Hangul::class))

      hangul.pullCommitted().toString() shouldBe ""
      hangul.pullUncommitted().toString() shouldBe "왰"
    }

    "궤양 - 3중모음" {
      val hangul = Hangul()
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅜ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅓ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅑ", Hangul::class))
      hangul.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))

      hangul.pullCommitted().toString() shouldBe "궤"
      hangul.pullUncommitted().toString() shouldBe "양"
    }

    // character without jongsung
    "character with vowel starting with 'ㅣ': 기" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "기"
      }
    }

    "character with vowel starting with 'ㅣ': 니" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "니"
      }
    }

    "character with vowel starting with 'ㅣ': 디" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄷ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "디"
      }
    }

    "character with vowel starting with 'ㅣ': 리" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "리"
      }
    }

    "character with vowel starting with 'ㅣ': 미" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅁ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "미"
      }
    }

    "character with vowel starting with 'ㅣ': 비" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅂ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "비"
      }
    }

    "character with vowel starting with 'ㅣ': 시" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅅ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "시"
      }
    }

    "character with vowel starting with 'ㅣ': 이" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "이"
      }
    }

    "character with vowel starting with 'ㅣ': 지" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅈ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "지"
      }
    }

    "character with vowel starting with 'ㅣ': 치" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅊ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "치"
      }
    }

    "character with vowel starting with 'ㅣ': 키" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅋ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "키"
      }
    }

    "character with vowel starting with 'ㅣ': 티" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅌ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "티"
      }
    }

    "character with vowel starting with 'ㅣ': 피" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅍ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "피"
      }
    }

    "character with vowel starting with 'ㅣ': 히" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "히"
      }
    }

    "character with vowel starting with 'ㅣ': 끼" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄲ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "끼"
      }
    }

    "character with vowel starting with 'ㅣ': 띠" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄸ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "띠"
      }
    }

    "character with vowel starting with 'ㅣ': 삐" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅃ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "삐"
      }
    }

    "character with vowel starting with 'ㅣ': 씨" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅆ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "씨"
      }
    }

    "character with vowel starting with 'ㅣ': 찌" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "히"
      }
    }

    "character with vowel starting with 'ㅡ': 쯔" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅉ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "쯔"
      }
    }

    "character with vowel starting with 'ㆍ': 쩌" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅉ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "ㅉ·"
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "쩌"
      }
    }

    // characters whose jongsung is consonant or pair consonant
    "character with vowel ending with 'ㅣ': 익" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "익"
      }
    }

    "character with vowel ending with 'ㅣ': 인" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "인"
      }
    }

    "character with vowel ending with 'ㅣ': 읻" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄷ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "읻"
      }
    }

    "character with vowel ending with 'ㅣ': 일" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "일"
      }
    }

    "character with vowel ending with 'ㅣ': 임" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅁ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "임"
      }
    }

    "character with vowel ending with 'ㅣ': 입" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅂ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "입"
      }
    }

    "character with vowel ending with 'ㅣ': 잇" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅅ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "잇"
      }
    }

    "character with vowel ending with 'ㅣ': 잉" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "잉"
      }
    }

    "character with vowel ending with 'ㅣ': 잊" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅈ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "잊"
      }
    }

    "character with vowel ending with 'ㅣ': 잋" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅊ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "잋"
      }
    }

    "character with vowel ending with 'ㅣ': 잌" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅋ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "잌"
      }
    }

    "character with vowel ending with 'ㅣ': 잍" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅌ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "잍"
      }
    }

    "character with vowel ending with 'ㅣ': 잎" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅍ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "잎"
      }
    }

    "character with vowel ending with 'ㅣ': 잏" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "잏"
      }
    }

    "character with vowel ending with 'ㅣ': 읶" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄲ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "읶"
      }
    }

    "character with vowel ending with 'ㅣ': 있" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅆ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "있"
      }
    }

    "character with vowel ending with 'ㅡ': 윽" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "윽"
      }
    }

    "character with vowel ending with 'ㆍ': 악" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "악"
      }
    }

    // characters whose jongsung is double consonant
    "jongsung is 'ㄳ': 읷" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅅ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "읷"
      }
    }

    "jongsung is 'ㄵ': 읹" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅈ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "읹"
      }
    }

    "jongsung is 'ㄶ': 읺" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "읺"
      }
    }

    "jongsung is 'ㄺ': 읽" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "읽"
      }
    }

    "jongsung is 'ㄻ': 읾" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅁ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "읾"
      }
    }

    "jongsung is 'ㄼ': 읿" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅂ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "읿"
      }
    }

    "jongsung is 'ㄽ': 잀" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅅ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "잀"
      }
    }

    "jongsung is 'ㄾ': 잁" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅌ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "잁"
      }
    }

    "jongsung is 'ㄿ': 잂" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅍ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "잂"
      }
    }

    "jongsung is 'ㅀ': 잃" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "잃"
      }
    }

    "jongsung is 'ㅄ': 잆" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅂ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅅ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "잆"
      }
    }

    "word: 기지" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "기"
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅈ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "깆"
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe "기"
        pullUncommitted().toString() shouldBe "지"
      }
    }

    // chosung that can't come with jongsung
    // ㄸ ㅃ ㅉ
    "word: 기쁨" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "기"
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅃ", Hangul::class, null))
        pullCommitted().toString() shouldBe "기"
        pullUncommitted().toString() shouldBe "ㅃ"
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅁ", Hangul::class, null))
        pullCommitted().toString() shouldBe "기"
        pullUncommitted().toString() shouldBe "쁨"
      }
    }

    // word with jongsung in front character
    // jongsung is ㄱ ㄴ ㄹ ㅂ
    // use chun
    "word: 익살" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅅ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "읷"
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        pullCommitted().toString() shouldBe "익"
        pullUncommitted().toString() shouldBe "시"
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class, null))
        pullCommitted().toString() shouldBe "익"
        pullUncommitted().toString() shouldBe "살"
      }
    }

    // not use ceon
    "word: 길항" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "긿"
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class, null))
        pullCommitted().toString() shouldBe "길"
        pullUncommitted().toString() shouldBe "하"
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        pullCommitted().toString() shouldBe "길"
        pullUncommitted().toString() shouldBe "항"
      }
    }

    // jongsung equal chosung
    // use ceon
    "word: 암막" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅁ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "암"
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅁ", Hangul::class, null))
        pullCommitted().toString() shouldBe "암"
        pullUncommitted().toString() shouldBe "ㅁ"
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class, null))
        pullCommitted().toString() shouldBe "암"
        pullUncommitted().toString() shouldBe "막"
      }
    }

    // not use ceon
    "word: 임무" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅁ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "임"
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅁ", Hangul::class, null))
        pullCommitted().toString() shouldBe "임"
        pullUncommitted().toString() shouldBe "ㅁ"
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅜ", Hangul::class, null))
        pullCommitted().toString() shouldBe "임"
        pullUncommitted().toString() shouldBe "무"
      }
    }

    // remainder
    // use ceon
    "word: 빗장" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅂ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅅ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "빗"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅈ", Hangul::class, null))
        pullCommitted().toString() shouldBe "빗"
        pullUncommitted().toString() shouldBe "ㅈ"
        countConsumed() shouldBe 3
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class, null))
        pullCommitted().toString() shouldBe "빗"
        pullUncommitted().toString() shouldBe "장"
        countConsumed() shouldBe 3
      }
    }

    // not use ceon
    "word: 빗금" {
      composer.run {
        clearCommitted()
        clearUncommitted()
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅂ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅅ", Hangul::class, null))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "빗"
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class, null))
        pullCommitted().toString() shouldBe "빗"
        pullUncommitted().toString() shouldBe "ㄱ"
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class, null))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅁ", Hangul::class, null))
        pullCommitted().toString() shouldBe "빗"
        pullUncommitted().toString() shouldBe "금"
      }
    }
  }
}