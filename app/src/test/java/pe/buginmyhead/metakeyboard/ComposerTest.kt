package pe.buginmyhead.metakeyboard

import io.kotlintest.TestCase
import io.kotlintest.TestResult
import io.kotlintest.extensions.TestListener
import io.kotlintest.shouldBe
import io.kotlintest.specs.FreeSpec

internal fun primitiveComposerAfterTestListenerFun(composer: PrimitiveComposer) {
  composer.run {
    clearCommitted()
    clearUncommitted()
    pullCommitted().toString() shouldBe ""
    pullUncommitted().toString() shouldBe ""
  }
}

internal fun languageComposerAfterTestListenerFun(composer: LanguageComposer) {
  composer.run {
    primitiveComposerAfterTestListenerFun(this)
    countConsumed() shouldBe 0
  }
}

internal abstract class PrimitiveComposerTest<out T: PrimitiveComposer>(
  val composer: T
) : FreeSpec() {

  private val listOfTestListener = listOf(
    object : TestListener {
      override fun afterTest(testCase: TestCase, result: TestResult) {
        primitiveComposerAfterTestListenerFun(composer)
      }
    }
  )

  override fun listeners(): List<TestListener> = listOfTestListener
}

internal abstract class LanguageComposerTest<out T: LanguageComposer>(
  composer: T
) : PrimitiveComposerTest<T>(composer) {

  private val listOfTestListener = listOf(
    object : TestListener {
      override fun afterTest(testCase: TestCase, result: TestResult) {
        languageComposerAfterTestListenerFun(composer)
      }
    }
  )

  override fun listeners(): List<TestListener> = listOfTestListener
}